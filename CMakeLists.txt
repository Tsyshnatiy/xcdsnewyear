cmake_minimum_required(VERSION 3.5)
project(aicup2020)

# OS and compiler checks.
if(WIN32)
    add_definitions(-DWIN32)
    SET(PROJECT_LIBS Ws2_32.lib)
endif()

# Apple clang 12 which is used by Vladimir Tsyshnatiy does not support concepts! So switch to gcc
if(APPLE)
    set(CMAKE_C_COMPILER "/usr/local/Cellar/gcc/11.2.0/bin/gcc-11")
    set(CMAKE_CXX_COMPILER "/usr/local/Cellar/gcc/11.2.0/bin/g++-11")
endif()

if(WIN32)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /std:c++20")
else()
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++2a")
endif()

file(GLOB MODEL_HEADERS "bot/model/*.hpp")
file(GLOB MODEL_SRC "bot/model/*.cpp")

file(GLOB ALGO_HEADERS "bot/algo/*.hpp")
file(GLOB ALGO_SRC "bot/algo/*.cpp")

file(GLOB ECONOMICS_HEADERS "bot/economics/*.hpp")
file(GLOB ECONOMICS_SRC "bot/economics/*.cpp")

file(GLOB MILITARY_HEADERS "bot/military/*.hpp")
file(GLOB MILITARY_SRC "bot/military/*.cpp")

set(INTERNAL_HEADERS ${MODEL_HEADERS} ${ALGO_HEADERS} ${ECONOMICS_HEADERS} ${MILITARY_HEADERS})
set(INTERNAL_SRC ${MODEL_SRC} ${ALGO_SRC} ${ECONOMICS_SRC} ${MILITARY_SRC})

file(GLOB HEADERS "bot/*.hpp" ${INTERNAL_HEADERS})
SET_SOURCE_FILES_PROPERTIES(${HEADERS} PROPERTIES HEADER_FILE_ONLY TRUE)
file(GLOB SRC "bot/*.cpp" ${INTERNAL_SRC})
add_executable(aicup2020 ${HEADERS} ${SRC})

TARGET_LINK_LIBRARIES(aicup2020 ${PROJECT_LIBS})

