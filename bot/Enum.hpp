#ifndef _ENUM_HPP_
#define _ENUM_HPP_

#include <concepts>
#include <stdexcept>
#include <type_traits>

template <typename Type>
concept BoundedEnum = std::is_enum_v<Type> && Type::EnumFirst <= Type::EnumLast;

template <BoundedEnum Enum, std::integral Representation>
Enum to(const Representation rep)
{
    if (rep < Enum::EnumFirst || rep > Enum::EnumLast)
    {
        throw std::invalid_argument("Invalid conversion to enum type");
    }
    return static_cast<Enum>(rep);
}

#endif
