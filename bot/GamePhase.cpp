#include "GamePhase.hpp"

#include "model/PlayerView.hpp"

GamePhase::Phase GamePhase::getPhase(const PlayerView& playerView) const
{
    const auto currentTick = playerView.currentTick;

    if (currentTick < 100)
    {
        return 1u;
    }

    if (currentTick < 250)
    {
        return 2u;
    }

    if (currentTick < 400)
    {
        return 3u;
    }

    if (currentTick < 600)
    {
        return 4u;
    }

    if (currentTick < 800)
    {
        return 5u;
    }

    return 6u;
}