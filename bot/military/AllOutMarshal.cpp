#include "AllOutMarshal.hpp"

#include "../model/Action.hpp"
#include "../model/AttackAction.hpp"
#include "../model/AutoAttack.hpp"
#include "../model/EntityAction.hpp"
#include "../model/PlayerView.hpp"
#include "../model/MoveAction.hpp"

#include "../PointsOfInterest.hpp"

#include <cassert>

static constexpr size_t minArmySize = 20;

static std::optional<CellIndex> findEnemy(const PlayerView& playerView)
{
    constexpr EntityType primaryEnemyTypes[]
    {
        EntityType::BUILDER_BASE,
        EntityType::MELEE_BASE,
        EntityType::RANGED_BASE,
        EntityType::HOUSE,
        EntityType::TURRET,
    };

    constexpr EntityType secondaryEnemyTypes[]
    {
        EntityType::BUILDER_UNIT,
        EntityType::MELEE_UNIT,
        EntityType::RANGED_UNIT,
    };

    CellIndex enemyPosition;
    auto findSpecificEnemy = [&, enemyId = playerView.getEnemyPlayer().id](const auto& specificEnemyTypes)
    {
        for (const auto enemyType : specificEnemyTypes)
        {
            const auto enemyIds = playerView.getEntityIds(enemyId, enemyType);
            if (enemyIds.empty())
            {
                continue;
            }
            const auto* enemy = playerView.getEntityById(*enemyIds.cbegin());
            if (enemy && enemy->active)
            {
                enemyPosition = enemy->position;
                return true;
            }
        }

        // This means that there are no enemies on the map
        return false;
    };

    if (findSpecificEnemy(primaryEnemyTypes) || findSpecificEnemy(secondaryEnemyTypes))
    {
        return enemyPosition;
    }
    return std::nullopt;
}

AllOutMarshal::AllOutMarshal(const PointsOfInterest& poi)
    : m_poi(poi)
{
}

void AllOutMarshal::onTick(Action& tickActions, const PlayerView& playerView)
{
    calibrateTurrets(tickActions, playerView);

    const auto myMelee = playerView.getEntityIds(playerView.myId, MELEE_UNIT);
    const auto myRanged = playerView.getEntityIds(playerView.myId, RANGED_UNIT);
    const size_t myArmySize = myMelee.size() + myRanged.size();

    size_t hisArmySize;
    {
        const auto& enemy = playerView.getEnemyPlayer();
        const auto hisMelee = playerView.getEntityIds(enemy.id, MELEE_UNIT);
        const auto hisRanged = playerView.getEntityIds(enemy.id, RANGED_UNIT);
        hisArmySize = hisMelee.size() + hisRanged.size();
    }

    if (!m_attackInProgress && myArmySize < std::min(hisArmySize * 3 / 2, minArmySize))
    {
        return; // Must amass enough units first to initiate the attack
    }

    if (m_attackInProgress && myArmySize < std::min(hisArmySize * 2 / 3, minArmySize))
    {
        m_attackInProgress = false;

        auto factories = playerView.getEntityIds(playerView.myId, EntityType::MELEE_BASE);
        if (!factories.empty())
        {
            if (const auto creationPoint = m_poi.getCreationPoint(*factories.cbegin(), playerView))
            {
                auto moveAction = std::make_shared<MoveAction>(creationPoint.value(), true, true);
                auto autoAttack = std::make_shared<AutoAttack>(5, std::vector{ BUILDER_UNIT, MELEE_UNIT, RANGED_UNIT, TURRET });
                auto attack = std::make_shared<AttackAction>(nullptr, std::move(autoAttack));

                for (const EntityID id : myMelee)
                {
                    auto& entityAction = tickActions.entityActions[id];
                    entityAction.moveAction = moveAction;
                    entityAction.attackAction = attack;
                }
            }
        }

        factories = playerView.getEntityIds(playerView.myId, EntityType::RANGED_BASE);
        if (!factories.empty())
        {
            if (const auto creationPoint = m_poi.getCreationPoint(*factories.cbegin(), playerView))
            {
                auto moveAction = std::make_shared<MoveAction>(creationPoint.value(), true, true);
                auto autoAttack = std::make_shared<AutoAttack>(10, std::vector{ BUILDER_UNIT, MELEE_UNIT, RANGED_UNIT, TURRET });
                auto attack = std::make_shared<AttackAction>(nullptr, std::move(autoAttack));

                for (const EntityID id : myRanged)
                {
                    auto& entityAction = tickActions.entityActions[id];
                    entityAction.moveAction = moveAction;
                    entityAction.attackAction = attack;
                }
            }
        }

        return;
    }

    const auto enemyPosition = findEnemy(playerView);
    if (!enemyPosition)
    {
        return; // No enemies on the map
    }

    m_attackInProgress = true;

    auto moveAction = std::make_shared<MoveAction>(enemyPosition.value(), true, true);
    auto autoAttack = std::make_shared<AutoAttack>(playerView.mapSize, std::vector{BUILDER_UNIT, MELEE_UNIT, RANGED_UNIT, TURRET});
    auto attack = std::make_shared<AttackAction>(nullptr, std::move(autoAttack));
    for (const EntityID id : myMelee)
    {
        auto& entityAction = tickActions.entityActions[id];
        entityAction.moveAction = moveAction;
        entityAction.attackAction = attack;
    }
    for (const EntityID id : myRanged)
    {
        auto& entityAction = tickActions.entityActions[id];
        entityAction.moveAction = moveAction;
        entityAction.attackAction = attack;
    }
}

void AllOutMarshal::calibrateTurrets(Action& tickActions, const PlayerView& playerView)
{
    auto nearbyAutoAttack = std::make_shared<AutoAttack>(6, std::vector{BUILDER_UNIT, MELEE_UNIT, RANGED_UNIT, TURRET});
    auto turretAttack = std::make_shared<AttackAction>(nullptr, std::move(nearbyAutoAttack));
    for (const EntityID id : playerView.getEntityIds(playerView.myId, TURRET))
    {
        tickActions.entityActions[id].attackAction = turretAttack;
    }
}
