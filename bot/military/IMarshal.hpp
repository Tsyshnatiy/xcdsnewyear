#ifndef _MILITARY_MARSHAL_HPP_
#define _MILITARY_MARSHAL_HPP_

class Action;
class PlayerView;

class IMarshal
{
public:
    virtual ~IMarshal() = default;

    virtual void onTick(Action& tickActions,
                        const PlayerView& playerView) = 0;
};

#endif
