#ifndef _MILITARY_ALL_OUT_MARSHAL_HPP_
#define _MILITARY_ALL_OUT_MARSHAL_HPP_

#include "IMarshal.hpp"

#include "../model/CellIndex.hpp"

#include <optional>

class PointsOfInterest;

class AllOutMarshal : public IMarshal
{
public:
    explicit AllOutMarshal(const PointsOfInterest& poi);

    void onTick(Action& tickActions, const PlayerView& playerView) override;

private:
    static void calibrateTurrets(Action& tickActions, const PlayerView& playerView);

    const PointsOfInterest& m_poi;
    bool m_attackInProgress = false;
};

#endif
