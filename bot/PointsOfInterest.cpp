#include "PointsOfInterest.hpp"

#include "algo/MapHelpers.hpp"

#include "model/Entity.hpp"
#include "model/EntityProperties.hpp"
#include "model/PlayerView.hpp"

#include <cassert>

std::optional<CellIndex> PointsOfInterest::getCreationPoint(EntityID baseId, const PlayerView& playerView) const
{
    const auto* base = playerView.getEntityById(baseId);
    if (!base || !base->active)
    {
        return std::nullopt;
    }

    const auto basePosition = base->position;
    const auto entityProps = playerView.entityProperties.at(base->entityType);

    // What if cell is a resource, outside of a map, or occupied by another building?
    return CellIndex{basePosition.x + entityProps.size, basePosition.y + entityProps.size - 1};
}

void PointsOfInterest::initRallyPoint(const PlayerView& playerView)
{
    const auto turretIds = playerView.getEntityIds(playerView.myId, EntityType::TURRET); // We always have a turret at the beginning
    assert(turretIds.size() == 1);

    const auto turret = playerView.getEntityById(*(turretIds.begin()));
    assert(turret != nullptr);
    m_rallyPoint = turret->position;
}

std::optional<CellIndex> PointsOfInterest::getOrCreateRallyPoint(const PlayerView& playerView)
{
    if (!m_rallyPoint)
    {
        initRallyPoint(playerView);
    }
    return m_rallyPoint;
}
