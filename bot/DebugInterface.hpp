#ifndef _DEBUG_INTERFACE_HPP_
#define _DEBUG_INTERFACE_HPP_

#include <memory>

class InputStream;
class OutputStream;
class DebugCommand;
class DebugState;

class DebugInterface {
public:
    DebugInterface(std::shared_ptr<InputStream> inputStream, std::shared_ptr<OutputStream> outputStream);
    void send(const DebugCommand& command);
    DebugState getState();

private:
    std::shared_ptr<InputStream> inputStream;
    std::shared_ptr<OutputStream> outputStream;
};

#endif
