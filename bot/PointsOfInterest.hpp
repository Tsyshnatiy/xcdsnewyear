#ifndef _BASECREATIONPOINTS_HPP_
#define _BASECREATIONPOINTS_HPP_

#include "model/Id.hpp"
#include "model/CellIndex.hpp"

#include <optional>

class PlayerView;

class PointsOfInterest final
{
public:
    std::optional<CellIndex> getCreationPoint(EntityID baseId, const PlayerView& playerView) const;
    std::optional<CellIndex> getOrCreateRallyPoint(const PlayerView& playerView);

private:
    void initRallyPoint(const PlayerView& playerView);
    
private:
    std::optional<CellIndex> m_rallyPoint;
};

#endif
