#ifndef _MY_STRATEGY_HPP_
#define _MY_STRATEGY_HPP_

#include "model/Action.hpp"

#include "PointsOfInterest.hpp"
#include "economics/Treasury.hpp"

#include <memory>

class DebugInterface;
class PlayerView;

class IProduction;
class IHarvester;
class IBuilder;
class IMarshal;

class MyStrategy {
public:
    MyStrategy();
    ~MyStrategy();
    Action getAction(const PlayerView& playerView, DebugInterface* debugInterface);
    void debugUpdate(const PlayerView& playerView, DebugInterface& debugInterface);

private:
    void invalidateActionsCache(const PlayerView& playerView);
    void copyActionsToCache(const Action& tickActions);

    void sendNewUnitsToRallyPoint(Action& tickActions, const PlayerView& playerView);

    auto getAutoAttackTargetsForCombatUnits() const;

    PointsOfInterest m_poi;
    Action m_actionsCache;
    Treasury m_treasury;

    std::unique_ptr<IProduction> m_production;
    std::unique_ptr<IHarvester> m_harvester;
    std::unique_ptr<IBuilder> m_builder;
    std::unique_ptr<IMarshal> m_marshal;
};

#endif
