#include "Harvester.hpp"

#include "../model/PlayerView.hpp"
#include "../model/EntityAction.hpp"
#include "../model/AttackAction.hpp"
#include "../model/AutoAttack.hpp"
#include "../model/Action.hpp"
#include "../model/Entity.hpp"

Harvester::Harvester(const Action& currentActions)
    : m_currentActions{currentActions}
{

}

void Harvester::onTick(Action& tickActions, const PlayerView& playerView)
{
    commandSlaves(tickActions, playerView);
}

void Harvester::commandSlaves(Action& tickActions, const PlayerView& playerView) const
{
    const auto slaveIDs = playerView.getEntityIds(playerView.myId, EntityType::BUILDER_UNIT);
    const auto& allMyEntities = playerView.getEntities(playerView.myId);

    auto autoAttack = std::make_shared<AutoAttack>(playerView.mapSize, std::vector{ EntityType::RESOURCE });
    const auto attack = std::make_shared<AttackAction>(nullptr, std::move(autoAttack));
    for (const auto slaveID : slaveIDs)
    {
        const auto& slave = allMyEntities.at(slaveID);
        if (!slave.active || m_currentActions.isBusy(slave.id))
        {
            continue;
        }

        EntityAction slaveAction;
        slaveAction.attackAction = attack;

        tickActions.entityActions[slave.id] = std::move(slaveAction);
    }
}