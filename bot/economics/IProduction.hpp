#ifndef _IPRODUCTION_HPP_
#define _IPRODUCTION_HPP_

class Action;
class PlayerView;
struct Population;
class Treasury;

class IProduction
{
public:
    virtual ~IProduction() = default;

    virtual void onTick(Action& tickActions,
                        const PlayerView& playerView,
                        const Treasury& treasury,
                        Population population) = 0;
};

#endif
