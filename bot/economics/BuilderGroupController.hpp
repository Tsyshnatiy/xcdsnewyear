#ifndef _BUILDER_GROUP_CONTROLLER_HPP_
#define _BUILDER_GROUP_CONTROLLER_HPP_

#include "../model/Id.hpp"
#include "../model/EntityType.hpp"
#include "../model/CellIndex.hpp"

#include "ConstructionState.hpp"
#include "BuilderGroup.hpp"

#include <optional>
#include <deque>

class PlayerView;
class Action;
class Treasury;
class Entity;

class BuilderGroupController final
{
public:
    BuilderGroupController(const Action& allCurrentActions, BuilderGroup& builderGroup);

    void onTick(Action& tickActions, const PlayerView& playerView, Treasury& treasury);

    void takeOrder(EntityType buildingType, OrderID orderId);
    bool isBusy() const;

private:
    void moveToConstructionSite(Action& tickActions, const PlayerView& playerView);
    void zeroGroupActions(Action& tickActions) const;

    void establishBuilding(Action& tickActions, const PlayerView& playerView, Treasury& treasury);
    std::optional<EntityID> getEstablishedBuilding(EntityType type, const PlayerView& playerView) const;
    void repairUntilFinished(Action& tickActions, const PlayerView& playerView);
    void freeBuilderGroup(Action& tickActions);

    void followTheForeman(Action& tickActions, const PlayerView& playerView) const;

    const Entity* getForeman(const PlayerView& playerView) const;

    bool isForemanStuck() const;

private:
    const Action& m_allCurrentActions;
    BuilderGroup& m_builderGroup;

    std::deque<CellIndex> m_lastForemanPositions;
    static constexpr std::size_t m_foremanPositionsBufferSize = 5;
};

#endif
