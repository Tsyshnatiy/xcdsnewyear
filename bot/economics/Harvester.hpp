#ifndef _HARVESTER_HPP_
#define _HARVESTER_HPP_

#include "IHarvester.hpp"

class Action;

class Harvester final : public IHarvester
{
public:
    Harvester(const Action& currentActions);

    void onTick(Action& tickActions, const PlayerView& playerView) override;

private:
    void commandSlaves(Action& tickActions, const PlayerView& playerView) const;

private:
    const Action& m_currentActions;
};

#endif
