#ifndef _NAIVE_PRODUCTION_HPP_
#define _NAIVE_PRODUCTION_HPP_

#include "IProduction.hpp"
#include "Population.hpp"

#include "../model/EntityType.hpp"

class Treasury;

class Production final : public IProduction
{
public:
    Production(const class PointsOfInterest& poi);

    void onTick(Action& tickActions,
                const PlayerView& playerView,
                 // Does not treasury amount for now. It is complex to correctly count money
                 // for instantly created unit
                const Treasury& treasury,
                Population population) override;

private:
    void createUnit(Action& tickActions,
                    const PlayerView& playerView,
                    const Treasury& treasury,
                    const Population& population,
                    EntityType unitType) const;

    int computeUnitCost(EntityType type,
                        std::size_t thisTypeUnitsCount,
                        const PlayerView& playerView) const;

    bool canProduce(EntityType type,
                    const Population& population,
                    const Treasury& treasury,
                    std::size_t thisTypeUnitsCount,
                    const PlayerView& playerView) const;

    void stopAllProduction(Action& tickActions, const PlayerView& playerView) const;

private:
    const PointsOfInterest& m_poi;
};

#endif
