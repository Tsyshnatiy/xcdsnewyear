#include "Builder.hpp"

#include "../model/Action.hpp"
#include "../model/EntityProperties.hpp"
#include "../model/PlayerView.hpp"

#include "Treasury.hpp"

Builder::Builder(const Action& currentActions)
    : m_builderGroup{createBuilderGroup()}, 
    m_builderGroupController{currentActions, m_builderGroup},
    m_currentActions{currentActions}
{
    m_currentOrders[EntityType::HOUSE] = 0;
}

BuilderGroup Builder::createBuilderGroup() const
{
    BuilderGroup result;
    result.constructionState = ConstructionState::None;
    result.buildingId = std::nullopt;
    result.currentOrder = std::nullopt;
    result.groupSize = 2u;
    result.slaveIds = {};

    return result;
}

void Builder::onTick(Action& tickActions,
                          Population population,
                          Treasury& treasury,
                          const PlayerView& playerView)
{
    buildHouse(tickActions, population, treasury, playerView);

    m_builderGroupController.onTick(tickActions, playerView, treasury);
}

void Builder::buildHouse(Action& tickActions,
                             Population population,
                             Treasury& treasury,
                             const PlayerView& playerView)
{
    // We should build houses "in advance"
    const auto houseProps = playerView.entityProperties.at(EntityType::HOUSE);
    if (population.used + getMinAvailPopulation() < population.max)
    {
        return;
    }

    const auto orderId = m_currentOrders.at(EntityType::HOUSE);

    // Save money for a house
    treasury.reserveMoney(houseProps.initialCost, orderId);

    if (!treasury.isEnoughMoneyFor(orderId, playerView))
    {
        return;
    }
    m_builderGroupController.takeOrder(EntityType::HOUSE, orderId);
}

std::size_t Builder::getMinAvailPopulation() const
{
    return 7u;
}
