#include "BuilderGroupComposer.hpp"

#include "SlaveFinder.hpp"

#include "../model/PlayerView.hpp"

#include <cassert>

BuilderGroupComposer::BuilderGroupComposer(const Action& allCurrentActions, const PlayerView& playerView)
    : m_allCurrentActions{allCurrentActions}, 
    m_playerView{playerView}
{

}

void BuilderGroupComposer::refill(BuilderGroup& group) const
{
    if (group.groupSize == 0)
    {
        return;
    }

    std::size_t actualGroupSize = 0;
    for (const auto helperId : group.slaveIds)
    {
        if (!m_playerView.isUnitDead(helperId))
        {
            ++actualGroupSize;
        }
    }

    if (actualGroupSize < group.groupSize)
    {
        const SlaveFinder slaveFinder{m_allCurrentActions, m_playerView};
        auto harvesterIds = slaveFinder.findNHarvesters(group.groupSize - actualGroupSize, group.slaveIds);
        group.slaveIds.insert(group.slaveIds.end(), harvesterIds.begin(), harvesterIds.end());
    }
}
