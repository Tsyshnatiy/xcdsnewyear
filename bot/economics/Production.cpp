#include "Production.hpp"
#include "UnitFactory.hpp"

#include "../model/Entity.hpp"
#include "../model/EntityProperties.hpp"
#include "../model/PlayerView.hpp"

#include "../GamePhase.hpp"

#include "Treasury.hpp"

#include <cassert>

static EntityType getFactoryType(EntityType unitType)
{
    switch (unitType)
    {
    case EntityType::BUILDER_UNIT: return EntityType::BUILDER_BASE;
    case EntityType::MELEE_UNIT:   return EntityType::MELEE_BASE;
    case EntityType::RANGED_UNIT:  return EntityType::RANGED_BASE;
    }

    assert(!"No factory for requested unit type");
    return EntityType();
}

static std::size_t getNeededWorkersCount(const PlayerView& playerView)
{
    GamePhase gamePhase;
    const auto phase = gamePhase.getPhase(playerView);
    switch (phase)
    {
    case 1u: return 10u;
    case 2u: return 15u;
    case 3u: return 17u;
    case 4u: return 20u;
    default: return 22u;
    }
}

Production::Production(const PointsOfInterest& poi)
    : m_poi{poi}
{

}

bool Production::canProduce(EntityType type, const Population& population,
                            const Treasury& treasury,
                            std::size_t thisTypeUnitsCount,
                            const PlayerView& playerView) const
{
    if (type == EntityType::BUILDER_UNIT && thisTypeUnitsCount >= getNeededWorkersCount(playerView))
    {
        return false;
    }

    const auto unitCost = computeUnitCost(type, thisTypeUnitsCount, playerView);
    if (!treasury.isEnoughFreeMoney(unitCost))
    {
        return false;
    }

    const auto neededUnitProps = playerView.entityProperties.at(type);
    return population.used + neededUnitProps.populationUse <= population.max;
}

int Production::computeUnitCost(EntityType type,
                                std::size_t thisTypeUnitsCount,
                                const PlayerView& playerView) const
{
    const auto unitProps = playerView.entityProperties.at(type);
    return unitProps.initialCost + static_cast<int>(thisTypeUnitsCount);
}

void Production::createUnit(Action& tickActions,
                            const PlayerView& playerView,
                            const Treasury& treasury,
                            const Population& population,
                            EntityType unitType) const
{
    UnitFactory unitFactory{m_poi, playerView};

    const auto factoryType = getFactoryType(unitType);
    const auto factories = playerView.getEntityIds(playerView.myId, factoryType);
    if (factories.empty())
    {
        return;
    }

    const auto myUnits = playerView.getEntityIds(playerView.myId, unitType);
    const auto baseId = *(factories.cbegin()); // first base
    if (!canProduce(unitType, population, treasury, myUnits.size(), playerView))
    {
        unitFactory.stopBaseProduction(tickActions, baseId);
        return;
    }

    unitFactory.createUnit(tickActions, baseId);
}

void Production::stopAllProduction(Action& tickActions, const PlayerView& playerView) const
{
    const auto stopProduction = [&, unitFactory = UnitFactory{ m_poi, playerView }](const EntityType baseType)
    {
        const auto baseIds = playerView.getEntityIds(playerView.myId, baseType);
        for (const auto baseId : baseIds)
        {
            unitFactory.stopBaseProduction(tickActions, baseId);
        }
    };

    stopProduction(EntityType::BUILDER_BASE);
    stopProduction(EntityType::MELEE_BASE);
    stopProduction(EntityType::RANGED_BASE);
}

void Production::onTick(Action& tickActions,
                        const PlayerView& playerView,
                        const Treasury& treasury,
                        Population population)
{
    stopAllProduction(tickActions, playerView);

    createUnit(tickActions, playerView, treasury, population, EntityType::BUILDER_UNIT);

    const auto myMelee = playerView.getEntityIds(playerView.myId, EntityType::MELEE_UNIT);
    const auto myArchers = playerView.getEntityIds(playerView.myId, EntityType::RANGED_UNIT);

    const auto combatUnitType = myArchers.size() * 2 < myMelee.size() ? EntityType::RANGED_UNIT : EntityType::MELEE_UNIT;
    createUnit(tickActions, playerView, treasury, population, combatUnitType);
}