#include "BuilderGroupController.hpp"

#include "../model/PlayerView.hpp"
#include "../model/EntityProperties.hpp"
#include "../model/MoveAction.hpp"
#include "../model/BuildAction.hpp"
#include "../model/RepairAction.hpp"
#include "../model/EntityAction.hpp"
#include "../model/Action.hpp"

#include "../algo/Navigation.hpp"

#include "BuilderGroupComposer.hpp"
#include "Treasury.hpp"

#include <cassert>
#include <iostream>

BuilderGroupController::BuilderGroupController(const Action& allCurrentActions, BuilderGroup& builderGroup)
    : m_allCurrentActions{allCurrentActions},
    m_builderGroup{builderGroup}
{

}

void BuilderGroupController::onTick(Action& tickActions, const PlayerView& playerView, Treasury& treasury)
{
    BuilderGroupComposer groupComposer{m_allCurrentActions, playerView};
    groupComposer.refill(m_builderGroup);

    switch(m_builderGroup.constructionState)
    {
        case ConstructionState::None: 
            return; // No orders, state is none - group is free
        case ConstructionState::GotOrder:
            zeroGroupActions(tickActions);
            moveToConstructionSite(tickActions, playerView);
            break;
        case ConstructionState::Moving:
            establishBuilding(tickActions, playerView, treasury);
            break;
        case ConstructionState::Established:
            followTheForeman(tickActions, playerView);
            repairUntilFinished(tickActions, playerView);
            break;
        case ConstructionState::Finished:
            freeBuilderGroup(tickActions);
            break;
        default:
            assert("No other stages");
    }
}

const Entity* BuilderGroupController::getForeman(const PlayerView& playerView) const
{
    const auto foremanId = m_builderGroup.getForemanId();
    if (!foremanId)
    {
        return nullptr; // Build is impossible - no foreman. Wait for refill from composer;
    }

    const auto* foreman = playerView.getEntityById(foremanId.value());
    if (!foreman || !foreman->active)
    {
        return nullptr; // Should be unreachible because Composer refills and actualizes group
    }

    return foreman;
}

void BuilderGroupController::takeOrder(EntityType buildingType, OrderID orderId)
{
    // Refuse taking orders if busy
    if (isBusy())
    {
        return;
    }

    m_builderGroup.constructionState = ConstructionState::GotOrder;
    m_builderGroup.currentOrder = buildingType;
    m_builderGroup.orderId = orderId;
}

bool BuilderGroupController::isBusy() const
{
    return m_builderGroup.currentOrder && m_builderGroup.constructionState != ConstructionState::None;
}

void BuilderGroupController::zeroGroupActions(Action& tickActions) const
{
    for (const auto id : m_builderGroup.slaveIds)
    {
        tickActions.entityActions[id] = EntityAction{};
    }
}

void BuilderGroupController::moveToConstructionSite(Action& tickActions, const PlayerView& playerView)
{
    if (!m_builderGroup.currentOrder || m_builderGroup.constructionState != ConstructionState::GotOrder)
    {
        return;
    }

    const auto* foreman = getForeman(playerView);
    if (!foreman)
    {
        // Composer will assign new foreman next tick
        return;
    }

    const auto orderType = m_builderGroup.currentOrder.value();

    const auto props = playerView.entityProperties.at(orderType);
    const auto corridorOffset = 2u;
    Navigation nav{playerView};
    const auto cellIndex = nav.getEmptyAreaAround(foreman->position, props.size + corridorOffset);
    if (!cellIndex)
    {
        return; // no place to build. Wait for map changes
    }

    auto actualBuildingPos = cellIndex.value();
    ++actualBuildingPos.x; // Take corridor offsets into account
    ++actualBuildingPos.y;

    auto moveAction = std::make_shared<MoveAction>(actualBuildingPos, true, true);
    auto buildAction = std::make_shared<BuildAction>(orderType, actualBuildingPos);

    tickActions.entityActions[foreman->id].buildAction = std::move(buildAction);
    tickActions.entityActions[foreman->id].moveAction = std::move(moveAction);

    m_builderGroup.constructionState = ConstructionState::Moving;
}

bool BuilderGroupController::isForemanStuck() const
{
    if (m_lastForemanPositions.empty())
    {
        return false;
    }

    const auto firstPos = m_lastForemanPositions.front();
    for (const auto cellIndex : m_lastForemanPositions)
    {
        if (firstPos != cellIndex)
        {
            return false;
        }
    }

    return true;
}

void BuilderGroupController::establishBuilding(Action& tickActions, const PlayerView& playerView, Treasury& treasury)
{
    const auto foremanStuckCrutch = [this, &playerView]
    {
        const auto* foreman = getForeman(playerView);
        if (foreman)
        {
            m_lastForemanPositions.push_back(foreman->position);
            if (m_lastForemanPositions.size() > m_foremanPositionsBufferSize)
            {
                m_lastForemanPositions.pop_front();
            }

            if (isForemanStuck())
            {
                std::cout << "Foreman is stuck" << std::endl;
                m_builderGroup.constructionState = ConstructionState::GotOrder;
            }
        }
    };

    assert(m_builderGroup.currentOrder); // We could not get to establish stage without order set
    m_builderGroup.buildingId = getEstablishedBuilding(m_builderGroup.currentOrder.value(), playerView);
    if (!m_builderGroup.buildingId)
    {
        foremanStuckCrutch();
        return;
    }

    assert(m_builderGroup.currentOrder);
    assert(m_builderGroup.orderId);

    const auto order = m_builderGroup.currentOrder.value();
    const auto props = playerView.entityProperties.at(order);

    m_builderGroup.constructionState = ConstructionState::Established;
    treasury.freeReservedMoney(props.initialCost, m_builderGroup.orderId.value());

    const auto foremanId = m_builderGroup.getForemanId();
    if (foremanId)
    {
        tickActions.entityActions[foremanId.value()].buildAction = nullptr;
    }
}

std::optional<EntityID> BuilderGroupController::getEstablishedBuilding(EntityType type, const PlayerView& playerView) const
{
    const auto idsOfType = playerView.getEntityIds(playerView.myId, type);

    for (const auto id : idsOfType)
    {
        const auto* entity = playerView.getEntityById(id);
        if (entity && !entity->active)
        {
            return id;
        }
    }

    return std::nullopt;
}

void BuilderGroupController::repairUntilFinished(Action& tickActions, const PlayerView& playerView)
{
    if (!m_builderGroup.buildingId)
    {
        m_builderGroup.constructionState = ConstructionState::GotOrder;
        return;
    }

    const auto* building = playerView.getEntityById(m_builderGroup.buildingId.value());
    if (!building)
    {
        m_builderGroup.constructionState = ConstructionState::GotOrder;
        return;
    }

    const auto props = playerView.entityProperties.at(building->entityType);
    const auto pos = building->position;
    const auto size = props.size;
    const CellIndex center {pos.x + size / 2, pos.y + size / 2};

    auto moveAction = std::make_shared<MoveAction>(center, true, true);
    auto repairAction = std::make_shared<RepairAction>(m_builderGroup.buildingId.value());
    for (const auto id : m_builderGroup.slaveIds)
    {
        tickActions.entityActions[id].repairAction = repairAction;
        tickActions.entityActions[id].moveAction = moveAction;
    }

    if (building->active)
    {
        m_builderGroup.constructionState = ConstructionState::Finished;
    }
}

void BuilderGroupController::freeBuilderGroup(Action& tickActions)
{
    for (const auto id : m_builderGroup.slaveIds)
    {
        tickActions.entityActions[id] = EntityAction{};
    }

    m_builderGroup.constructionState = ConstructionState::None;
    m_builderGroup.currentOrder = std::nullopt;
    m_builderGroup.buildingId = std::nullopt;
    m_builderGroup.orderId = std::nullopt;
}

void BuilderGroupController::followTheForeman(Action& tickActions, const PlayerView& playerView) const
{
    const auto foremanId = m_builderGroup.getForemanId();
    if (!foremanId)
    {
        return;
    }

    const auto* foreman = playerView.getEntityById(foremanId.value());
    if (!foreman || !foreman->active)
    {
        return;
    }

    const auto moveAction = std::make_shared<MoveAction>(foreman->position, true, true);
    for (const auto id : m_builderGroup.slaveIds)
    {
        if (id == foremanId.value())
        {
            continue;
        }

        tickActions.entityActions[id].moveAction = moveAction;
    }
}
