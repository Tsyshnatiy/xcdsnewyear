#ifndef _POPULATION_HPP_
#define _POPULATION_HPP_

#include <cstddef>

struct Population final
{
    std::size_t used = 0;
    std::size_t max = 0;
};

#endif
