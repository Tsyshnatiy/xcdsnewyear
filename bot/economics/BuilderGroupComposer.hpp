#ifndef _BUILDER_GROUP_COMPOSER_HPP_
#define _BUILDER_GROUP_COMPOSER_HPP_

#include "../model/Id.hpp"

#include "BuilderGroup.hpp"

#include <optional>

class PlayerView;
class Action;

class BuilderGroupComposer final
{
public:
    BuilderGroupComposer(const Action& allCurrentActions, const PlayerView& playerView);

    // Those methods will try to assign as many slaves to the group as they can
    void refill(BuilderGroup& group) const;

private:
    const Action& m_allCurrentActions;
    const PlayerView& m_playerView;
};

#endif
