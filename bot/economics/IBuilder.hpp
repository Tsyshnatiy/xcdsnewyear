#ifndef _IBUILDER_HPP_
#define _IBUILDER_HPP_

#include "Population.hpp"

class Action;
class PlayerView;
class Treasury;

class IBuilder
{
public:
    virtual ~IBuilder() = default;

    virtual void onTick(Action& tickActions,
                        Population population,
                        Treasury& treasury,
                        const PlayerView& playerView) = 0;

    // That should depend on a game's phase
    virtual std::size_t getMinAvailPopulation() const = 0;
};

#endif
