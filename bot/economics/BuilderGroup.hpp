#ifndef _BUILDERGROUP_HPP_
#define _BUILDERGROUP_HPP_

#include "../model/Id.hpp"
#include "../model/EntityType.hpp"

#include "ConstructionState.hpp"

#include <vector>
#include <optional>
#include <cstddef>

struct BuilderGroup final
{
    std::vector<EntityID> slaveIds;
    std::size_t groupSize = 0;

    std::optional<EntityID> buildingId;
    std::optional<EntityType> currentOrder;
    std::optional<OrderID> orderId;
    
    ConstructionState constructionState = ConstructionState::None;

    std::optional<EntityID> getForemanId() const 
    { 
        if (slaveIds.empty())
        {
            return std::nullopt;
        }

        return slaveIds.front();
    }
};

#endif
