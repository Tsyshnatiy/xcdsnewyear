#ifndef _POPULATION_COMPUTER_HPP_
#define _POPULATION_COMPUTER_HPP_

#include <cstddef>

#include "Population.hpp"

class PlayerView;

class PopulationComputer final
{
public:
    PopulationComputer(const PlayerView& playerView);

    Population compute() const;

private:
    const PlayerView& m_playerView;
};

#endif
