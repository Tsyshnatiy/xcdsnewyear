#ifndef _TREASURY_HPP_
#define _TREASURY_HPP_

#include "../model/Id.hpp"

#include <unordered_map>

class PlayerView;

class Treasury final
{
public:
    void onTick(const PlayerView& playerView);

    bool isEnoughMoneyFor(OrderID orderId, const PlayerView& playerView) const;
    bool isEnoughFreeMoney(int price) const;
    
    // Call to start save money for something
    void reserveMoney(int amount, OrderID orderId);

    // Call to tell that reserved money are actually spent
    void freeReservedMoney(int amount, OrderID orderId);

private:
    std::unordered_map<OrderID, int> m_reservedMoney;
    int m_freeMoney = 0; // Can be negative
};

#endif
