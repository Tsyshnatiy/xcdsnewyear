#include "SlaveFinder.hpp"

#include "../model/Entity.hpp"
#include "../model/PlayerView.hpp"
#include "../model/Action.hpp"
#include "../model/EntityAction.hpp"
#include "../model/AttackAction.hpp"
#include "../model/AutoAttack.hpp"

SlaveFinder::SlaveFinder(const Action& allCurrentActions, const PlayerView& playerView)
    : m_allCurrentActions{allCurrentActions},
    m_playerView{playerView}
{

}

bool SlaveFinder::isHarvester(EntityID slaveId) const
{
    const auto* slave = m_playerView.getEntityById(slaveId);
    if (!slave || !slave->active)
    {
        return false;
    }

    auto slaveActionIt = m_allCurrentActions.entityActions.find(slaveId);
    if (slaveActionIt == m_allCurrentActions.entityActions.cend())
    {
        return false;
    }

    const auto& slaveAction = slaveActionIt->second;
    if (!slaveAction.attackAction || !slaveAction.attackAction->autoAttack) 
    {
        return false;
    }

    const auto autoAttack = *(slaveAction.attackAction->autoAttack);
    return autoAttack.validTargets.size() == 1 && autoAttack.validTargets.front() == EntityType::RESOURCE;
}

std::vector<EntityID> SlaveFinder::findNHarvesters(std::size_t n, const std::vector<EntityID>& ignoreList) const
{
    std::vector<EntityID> result;

    const auto slaveIds = m_playerView.getEntityIds(m_playerView.myId, EntityType::BUILDER_UNIT);

    for (const auto id : slaveIds)
    {
        if (n == 0)
        {
            break;
        }

        if (std::count(ignoreList.cbegin(), ignoreList.cend(), id) != 0)
        {
            continue;
        }
        
        if (!isHarvester(id))
        {
            continue;
        }

        const auto slave = m_playerView.getEntityById(id);
        if (!slave || !slave->active)
        {
            continue;
        }

        result.push_back(id);
        --n;
    }

    return result;  
}