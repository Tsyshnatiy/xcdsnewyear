#ifndef _CONSTRUCTION_STATE_HPP_
#define _CONSTRUCTION_STATE_HPP_

enum class ConstructionState
{
    None,
    GotOrder,
    Moving,
    Established,
    Finished,
};

#endif
