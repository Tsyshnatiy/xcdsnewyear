#include "PopulationComputer.hpp"

#include "../model/Entity.hpp"
#include "../model/PlayerView.hpp"
#include "../model/EntityProperties.hpp"

PopulationComputer::PopulationComputer(const PlayerView& playerView)
    : m_playerView{playerView}
{}

Population PopulationComputer::compute() const
{
    Population result;
    const auto& allMyEntities = m_playerView.getEntities(m_playerView.myId);

    for (const auto& entry : allMyEntities)
    {
        const auto& entity = entry.second;
        if (!entity.active)
        {
            continue;
        }
        
        const auto entityProps = m_playerView.entityProperties.at(entity.entityType);

        result.used += entityProps.populationUse;
        result.max += entityProps.populationProvide;
    }

    return result;
}