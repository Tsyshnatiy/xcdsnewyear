#include "Treasury.hpp"

#include "../model/PlayerView.hpp"

#include <algorithm>

void Treasury::onTick(const PlayerView& playerView)
{
    const auto curResourceAmount = playerView.getMyPlayer().resource;

    int totalReservedMoney = 0;
    for (const auto& [orderId, amount] : m_reservedMoney)
    {
        totalReservedMoney += amount;
    }

    m_freeMoney = curResourceAmount - totalReservedMoney;
}

bool Treasury::isEnoughFreeMoney(int price) const
{
    return m_freeMoney >= price;
}

bool Treasury::isEnoughMoneyFor(OrderID orderId, const PlayerView& playerView) const
{
    if (!m_reservedMoney.contains(orderId))
    {
        return false;
    }

    const auto reservedAmount = m_reservedMoney.at(orderId);
    const auto curResourceAmount = playerView.getMyPlayer().resource;

    return curResourceAmount >= reservedAmount;
}

void Treasury::reserveMoney(int amount, OrderID orderId)
{
    if (m_reservedMoney.contains(orderId))
    {
        return; // Cannot reserve money for the same order twice
    }

    m_reservedMoney[orderId] = amount;
    m_freeMoney -= amount;
}

void Treasury::freeReservedMoney(int amount, OrderID orderId)
{
    if (!m_reservedMoney.contains(orderId))
    {
        return; // No order - nothing to do
    }

    // Reserved money are spent, order is executed
    m_reservedMoney.erase(orderId);
}
