#ifndef _NAIVE_BUILDER_HPP_
#define _NAIVE_BUILDER_HPP_

#include "IBuilder.hpp"
#include "Population.hpp"
#include "BuilderGroupController.hpp"
#include "BuilderGroup.hpp"

#include "../model/Id.hpp"
#include "../model/EntityType.hpp"

#include <unordered_map>

class Treasury;

class Builder final : public IBuilder
{
public:
    Builder(const Action& currentActions);

    void onTick(Action& tickActions,
                Population population,
                Treasury& treasury,
                const PlayerView& playerView) override;

    std::size_t getMinAvailPopulation() const override;

private:
    void buildHouse(Action& tickActions,
                   Population population,
                   Treasury& treasury,
                   const PlayerView& playerView);

    BuilderGroup createBuilderGroup() const;
    
private:
    // Can be multiple groups
    BuilderGroup m_builderGroup;
    BuilderGroupController m_builderGroupController;
    const Action& m_currentActions;

    std::unordered_map<EntityType, OrderID> m_currentOrders;
};

#endif
