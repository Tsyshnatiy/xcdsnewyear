#ifndef _UNIT_FACTORY_HPP_
#define _UNIT_FACTORY_HPP_

#include "../model/Id.hpp"

class Action;
class PlayerView;
class PointsOfInterest;

class UnitFactory
{
public:
    UnitFactory(const PointsOfInterest& poi, const PlayerView& playerView);

    void createUnit(Action& tickActions, EntityID baseID) const;
    void stopBaseProduction(Action& tickActions, EntityID baseID) const;

private:
    const PointsOfInterest& m_poi;
    const PlayerView& m_playerView;
};

#endif
