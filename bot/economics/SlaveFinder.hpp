#ifndef _SLAVE_FINDER_HPP_
#define _SLAVE_FINDER_HPP_

#include "../model/Id.hpp"

#include <vector>

class Entity;
class PlayerView;
class Action;

class SlaveFinder final
{
public:
    SlaveFinder(const Action& allCurrentActions, const PlayerView& playerView);
    
    bool isHarvester(EntityID slaveId) const;
    std::vector<EntityID> findNHarvesters(std::size_t n, const std::vector<EntityID>& ignoreList) const;

private:
    const Action& m_allCurrentActions;
    const PlayerView& m_playerView;
};

#endif
