#include "UnitFactory.hpp"

#include "../model/Action.hpp"
#include "../model/PlayerView.hpp"
#include "../model/Entity.hpp"
#include "../model/EntityProperties.hpp"
#include "../model//BuildProperties.hpp"
#include "../model/BuildAction.hpp"
#include "../model/EntityAction.hpp"

#include "../PointsOfInterest.hpp"

#include <cassert>

UnitFactory::UnitFactory(const PointsOfInterest& poi, const PlayerView& playerView)
    : m_poi{poi},
    m_playerView{playerView}
{

}

void UnitFactory::createUnit(Action& tickActions, EntityID baseID) const
{
    const auto* base = m_playerView.getEntityById(baseID);
    if (!base || !base->active)
    {
        return;
    }

    const auto basePosition = base->position;
    const auto baseProps = m_playerView.entityProperties.at(base->entityType);
    const auto& buildProps = baseProps.build;
    assert(buildProps != nullptr);
    assert(buildProps->options.size() == 1 || !"A base must produce only one unit type");

    const auto creationPoint = m_poi.getCreationPoint(base->id, m_playerView);
    if (!creationPoint || m_playerView.getEntityAtCell(creationPoint.value()))
    {
        return;
    }

    EntityAction entityAction;
    entityAction.buildAction = std::make_shared<BuildAction>(buildProps->options.front(), creationPoint.value());

    tickActions.entityActions[base->id] = std::move(entityAction);
}

void UnitFactory::stopBaseProduction(Action& tickActions, EntityID baseID) const
{
    const auto* base = m_playerView.getEntityById(baseID);
    if (!base || !base->active)
    {
        return;
    }

    tickActions.entityActions[base->id] = EntityAction{};
}