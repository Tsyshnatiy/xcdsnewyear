#ifndef _IHARVESTER_HPP_
#define _IHARVESTER_HPP_

class Action;
class PlayerView;

class IHarvester
{
public:
    virtual ~IHarvester() = default;

    virtual void onTick(Action& tickActions, const PlayerView& playerView) = 0;
};

#endif
