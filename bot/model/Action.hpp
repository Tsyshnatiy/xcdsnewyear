#ifndef _MODEL_ACTION_HPP_
#define _MODEL_ACTION_HPP_

#include "Id.hpp"
#include "EntityAction.hpp"

#include <unordered_map>

class Action {
public:
    std::unordered_map<EntityID, EntityAction> entityActions;

    bool isBusy(EntityID id) const;

    Action();
    Action(std::unordered_map<EntityID, EntityAction> entityActions);
    static Action readFrom(class InputStream& stream);
    void writeTo(class OutputStream& stream) const;
};

#endif
