#ifndef _MODEL_CLIENT_MESSAGE_HPP_
#define _MODEL_CLIENT_MESSAGE_HPP_

#include "Action.hpp"
#include "DebugCommand.hpp"
#include <memory>

class ClientMessage {
public:
    class DebugMessage;
    class ActionMessage;
    class DebugUpdateDone;
    class RequestDebugState;

    static std::shared_ptr<ClientMessage> readFrom(class InputStream& stream);
    virtual void writeTo(class OutputStream& stream) const = 0;
};

class ClientMessage::DebugMessage : public ClientMessage {
public:
    static const int TAG;
public:
    std::shared_ptr<DebugCommand> command;
    DebugMessage();
    DebugMessage(std::shared_ptr<DebugCommand> command);
    static DebugMessage readFrom(InputStream& stream);
    void writeTo(OutputStream& stream) const override;
};

class ClientMessage::ActionMessage : public ClientMessage {
public:
    static const int TAG;
public:
    Action action;
    ActionMessage();
    ActionMessage(Action action);
    static ActionMessage readFrom(InputStream& stream);
    void writeTo(OutputStream& stream) const override;
};

class ClientMessage::DebugUpdateDone : public ClientMessage {
public:
    static const int TAG;
public:
    DebugUpdateDone();
    static DebugUpdateDone readFrom(InputStream& stream);
    void writeTo(OutputStream& stream) const override;
};

class ClientMessage::RequestDebugState : public ClientMessage {
public:
    static const int TAG;
public:
    RequestDebugState();
    static RequestDebugState readFrom(InputStream& stream);
    void writeTo(OutputStream& stream) const override;
};

#endif
