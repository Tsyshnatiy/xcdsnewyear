#include "PlayerView.hpp"
#include "../Enum.hpp"
#include "../Stream.hpp"
#include "../Utility.hpp"
#include "Entity.hpp"
#include "EntityProperties.hpp"
#include <cassert>

PlayerView PlayerView::readFrom(InputStream& stream) {
    PlayerView result;
    result.myId = stream.readInt();
    auto mapSize = stream.readInt();
    assert(mapSize > 0);
    result.mapSize = static_cast<std::size_t>(mapSize);

    result.fogOfWar = stream.readBool();
    size_t entityPropertiesCount = stream.readInt();
    result.entityProperties.reserve(entityPropertiesCount);
    while (entityPropertiesCount--) {
        const auto type = to<EntityType>(stream.readInt());
        result.entityProperties.emplace(type, EntityProperties::readFrom(stream));
    }
    result.maxTickCount = stream.readInt();
    result.maxPathfindNodes = stream.readInt();
    result.currentTick = stream.readInt();
    size_t playersCount = stream.readInt();

    assert(playersCount == 2);

    result.player1 = Player::readFrom(stream);
    result.player2 = Player::readFrom(stream);

    size_t entitiesCount = stream.readInt();
    while (entitiesCount--)
    {
        auto e = Entity::readFrom(stream);
        if (!e.playerId)
        {
            result.resources[e.id] = std::move(e);
        }
        else if (*e.playerId == result.player1.id)
        {
            result.firstPlayerEntities[e.id] = std::move(e);
        }
        else if (*e.playerId == result.player2.id)
        {
            result.secondPlayerEntities[e.id] = std::move(e);
        }

        result.entityPositions[e.position] = e.id;
    }
    return result;
}
void PlayerView::writeTo(OutputStream& stream) const {
    stream.write(myId);
    stream.write(static_cast<int>(mapSize));
    stream.write(fogOfWar);
    stream.write((int)(entityProperties.size()));
    for (const auto& [type, properties] : entityProperties) {
        stream.write((int)(type));
        properties.writeTo(stream);
    }
    stream.write(maxTickCount);
    stream.write(maxPathfindNodes);
    stream.write(currentTick);
    stream.write(2); // players.size()
    player1.writeTo(stream);
    player2.writeTo(stream);

    stream.write((int)(firstPlayerEntities.size() + secondPlayerEntities.size() + resources.size()));
    for (const auto& entry : firstPlayerEntities) {
        entry.second.writeTo(stream);
    }
    for (const auto& entry : secondPlayerEntities) {
        entry.second.writeTo(stream);
    }
    for (const auto& entry : resources) {
        entry.second.writeTo(stream);
    }
}

std::unordered_map<EntityID, Entity>& PlayerView::PlayerView::getEntities(std::optional<int> playerId)
{
    if (!playerId)
    {
        return resources;
    }
    return player1.id == playerId.value() ? firstPlayerEntities : secondPlayerEntities;
}

const std::unordered_map<EntityID, Entity>& PlayerView::getEntities(std::optional<int> playerId) const
{
    if (!playerId)
    {
        return resources;
    }
    return player1.id == playerId.value() ? firstPlayerEntities : secondPlayerEntities;
}

std::unordered_set<EntityID> PlayerView::getEntityIds(int playerId, EntityType type) const
{
    std::unordered_set<EntityID> result;

    for (const auto& [id, entity] : getEntities(playerId))
    {
        if (entity.entityType == type)
        {
            result.insert(id);
        }
    }

    return result;
}

const Entity* PlayerView::getEntityAtCell(CellIndex cellIndex) const
{
    auto it = entityPositions.find(cellIndex);
    if (it == entityPositions.end())
    {
        return nullptr;
    }

    auto id = it->second;
    return getEntityById(id);
}

Entity* PlayerView::getEntityAtCell(CellIndex cellIndex)
{
    const auto* entity = const_this->getEntityAtCell(cellIndex);
    return const_cast<Entity*>(entity);
}

const Entity* PlayerView::getEntityById(EntityID id) const
{
    auto firstPlayerIt = firstPlayerEntities.find(id);
    if (firstPlayerIt != firstPlayerEntities.end())
    {
        return &firstPlayerIt->second;
    }

    auto secondPlayerIt = secondPlayerEntities.find(id);
    if (secondPlayerIt != secondPlayerEntities.end())
    {
        return &secondPlayerIt->second;
    }

    auto resourceIt = resources.find(id);
    if (resourceIt != resources.end())
    {
        return &resourceIt->second;
    }

    return nullptr;
}

Entity* PlayerView::getEntityById(EntityID id)
{
    const auto* entity = const_this->getEntityById(id);
    return const_cast<Entity*>(entity);
}

const Player& PlayerView::getMyPlayer() const
{
    return myId == player1.id ? player1 : player2;
}

const Player& PlayerView::getEnemyPlayer() const
{
    return myId == player1.id ? player2 : player1;
}

std::unordered_set<EntityID> PlayerView::getBuildingIds(std::optional<PlayerID> playerId) const
{
    std::unordered_set<EntityID> result;

    constexpr EntityType buildings[] = { BUILDER_BASE, TURRET, WALL, HOUSE, MELEE_BASE, RANGED_BASE };

    if (playerId)
    {
        for (const auto type : buildings)
        {
            auto buildingsOfType = getEntityIds(playerId.value(), type);
            result.insert(buildingsOfType.begin(), buildingsOfType.end());
        }
    }
    else
    {
        for (const auto type : buildings)
        {
            auto player1BuildingsOfType = getEntityIds(player1.id, type);
            auto player2BuildingsOfType = getEntityIds(player2.id, type);

            result.insert(player1BuildingsOfType.begin(), player1BuildingsOfType.end());
            result.insert(player2BuildingsOfType.begin(), player2BuildingsOfType.end());
        }
    }

    return result;
}

bool PlayerView::isUnitDead(EntityID id) const
{
    const auto* unit = getEntityById(id);
    return !unit || !unit->active;
}