#include "Entity.hpp"
#include "../Enum.hpp"
#include "../Stream.hpp"
#include "Vec2Int.hpp"

Entity::Entity(EntityID id, std::shared_ptr<PlayerID> playerId, EntityType entityType,
               CellIndex position, int health, bool active)
    : id(id),
      health(health),
      playerId(std::move(playerId)),
      position(position),
      entityType(entityType),
      active(active)
{

}

Entity Entity::readFrom(InputStream& stream) {
    Entity result;
    result.id = stream.readInt();
    auto* const playerId = stream.readBool() ? new auto(stream.readInt()) : nullptr;
    result.playerId.reset(playerId);

    result.entityType = to<EntityType>(stream.readInt());
    result.position = CellIndex::readFrom(stream);
    result.health = stream.readInt();
    result.active = stream.readBool();
    return result;
}
void Entity::writeTo(OutputStream& stream) const {
    stream.write(id);
    stream.write(!!playerId);
    if (playerId) {
        stream.write(*playerId);
    }
    stream.write((int)(entityType));
    position.writeTo(stream);
    stream.write(health);
    stream.write(active);
}
