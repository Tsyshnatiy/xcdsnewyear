#include "Action.hpp"
#include "../Stream.hpp"
#include "EntityAction.hpp"

Action::Action() = default;

Action::Action(std::unordered_map<int, EntityAction> entityActions) : entityActions(std::move(entityActions)) { }

Action Action::readFrom(InputStream& stream) {
    Action result;
    size_t entityActionsSize = stream.readInt();
    result.entityActions.reserve(entityActionsSize);
    while (entityActionsSize--) {
        const int key = stream.readInt();
        const EntityAction value = EntityAction::readFrom(stream);
        result.entityActions.emplace(key, value);
    }
    return result;
}
void Action::writeTo(OutputStream& stream) const {
    stream.write((int)(entityActions.size()));
    for (const auto& [key, value] : entityActions) {
        stream.write(key);
        value.writeTo(stream);
    }
}

bool Action::isBusy(EntityID id) const
{
    auto it =  entityActions.find(id);
    if (it == entityActions.cend())
    {
        return false;
    }

    const auto& entityAction = it->second;
    return entityAction.attackAction || entityAction.buildAction || entityAction.moveAction || entityAction.repairAction;
}
