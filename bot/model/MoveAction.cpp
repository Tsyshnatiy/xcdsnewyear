#include "MoveAction.hpp"
#include "../Stream.hpp"
#include "Hashing.hpp"

MoveAction::MoveAction(CellIndex target, bool findClosestPosition, bool breakThrough)
    : target(std::move(target)),
      findClosestPosition(findClosestPosition),
      breakThrough(breakThrough)
{ }

MoveAction MoveAction::readFrom(InputStream& stream) {
    MoveAction result;
    result.target = CellIndex::readFrom(stream);
    result.findClosestPosition = stream.readBool();
    result.breakThrough = stream.readBool();
    return result;
}
void MoveAction::writeTo(OutputStream& stream) const {
    target.writeTo(stream);
    stream.write(findClosestPosition);
    stream.write(breakThrough);
}
bool MoveAction::operator ==(const MoveAction& other) const {
    return target == other.target && findClosestPosition == other.findClosestPosition && breakThrough == other.breakThrough;
}
size_t std::hash<MoveAction>::operator ()(const MoveAction& value) const {
    return CombinedHash{} << value.target << value.findClosestPosition << value.breakThrough;
}
