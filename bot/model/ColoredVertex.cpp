#include "ColoredVertex.hpp"
#include "../Stream.hpp"
#include "../Utility.hpp"

ColoredVertex::ColoredVertex() = default;
ColoredVertex::ColoredVertex(std::shared_ptr<Vec2Float> worldPos, Vec2Float screenOffset, Color color) : worldPos(std::move(worldPos)), screenOffset(screenOffset), color(color) { }
ColoredVertex ColoredVertex::readFrom(InputStream& stream) {
    ColoredVertex result;
    auto* const worldPos = stream.readBool() ? new auto(Vec2Float::readFrom(stream)) : nullptr;
    result.worldPos.reset(worldPos);
    result.screenOffset = Vec2Float::readFrom(stream);
    result.color = Color::readFrom(stream);
    return result;
}
void ColoredVertex::writeTo(OutputStream& stream) const {
    stream.write(!!worldPos);
    do_if (worldPos)->writeTo(stream);
    screenOffset.writeTo(stream);
    color.writeTo(stream);
}
