#include "EntityAction.hpp"
#include "../Stream.hpp"
#include "../Utility.hpp"
#include "AttackAction.hpp"
#include "BuildAction.hpp"
#include "MoveAction.hpp"
#include "RepairAction.hpp"

EntityAction::EntityAction() = default;
EntityAction::EntityAction(std::shared_ptr<MoveAction> moveAction, std::shared_ptr<BuildAction> buildAction, std::shared_ptr<AttackAction> attackAction, std::shared_ptr<RepairAction> repairAction) : moveAction(std::move(moveAction)), buildAction(std::move(buildAction)), attackAction(std::move(attackAction)), repairAction(std::move(repairAction)) { }
EntityAction EntityAction::readFrom(InputStream& stream) {
    EntityAction result;
    auto* const moveAction = stream.readBool() ? new auto(MoveAction::readFrom(stream)) : nullptr;
    result.moveAction.reset(moveAction);
    auto* const buildAction = stream.readBool() ? new auto(BuildAction::readFrom(stream)) : nullptr;
    result.buildAction.reset(buildAction);
    auto* const attackAction = stream.readBool() ? new auto(AttackAction::readFrom(stream)) : nullptr;
    result.attackAction.reset(attackAction);
    auto* const repairAction = stream.readBool() ? new auto(RepairAction::readFrom(stream)) : nullptr;
    result.repairAction.reset(repairAction);
    return result;
}
void EntityAction::writeTo(OutputStream& stream) const {
    stream.write(!!moveAction);
    do_if (moveAction)->writeTo(stream);
    stream.write(!!buildAction);
    do_if (buildAction)->writeTo(stream);
    stream.write(!!attackAction);
    do_if (attackAction)->writeTo(stream);
    stream.write(!!repairAction);
    do_if (repairAction)->writeTo(stream);
}
