#include "ClientMessage.hpp"
#include "../Stream.hpp"
#include "EntityAction.hpp"
#include <array>

static constexpr std::array<std::shared_ptr<ClientMessage>(*)(InputStream& stream), 4> makeMessage{
    [](InputStream& stream) {
        return std::shared_ptr<ClientMessage>(new auto(ClientMessage::DebugMessage::readFrom(stream)));
    },
    [](InputStream& stream) {
        return std::shared_ptr<ClientMessage>(new auto(ClientMessage::ActionMessage::readFrom(stream)));
    },
    [](InputStream& stream) {
        return std::shared_ptr<ClientMessage>(new auto(ClientMessage::DebugUpdateDone::readFrom(stream)));
    },
    [](InputStream& stream) {
        return std::shared_ptr<ClientMessage>(new auto(ClientMessage::RequestDebugState::readFrom(stream)));
    },
};

std::shared_ptr<ClientMessage> ClientMessage::readFrom(InputStream& stream) {
    return makeMessage.at(stream.readInt())(stream);
}

const int ClientMessage::DebugMessage::TAG = 0;
ClientMessage::DebugMessage::DebugMessage() = default;
ClientMessage::DebugMessage::DebugMessage(std::shared_ptr<DebugCommand> command) : command(std::move(command)) { }
ClientMessage::DebugMessage ClientMessage::DebugMessage::readFrom(InputStream& stream) {
    ClientMessage::DebugMessage result;
    result.command = DebugCommand::readFrom(stream);
    return result;
}
void ClientMessage::DebugMessage::writeTo(OutputStream& stream) const {
    stream.write(TAG);
    command->writeTo(stream);
}

const int ClientMessage::ActionMessage::TAG = 1;
ClientMessage::ActionMessage::ActionMessage() = default;
ClientMessage::ActionMessage::ActionMessage(Action action) : action(std::move(action)) { }
ClientMessage::ActionMessage ClientMessage::ActionMessage::readFrom(InputStream& stream) {
    ClientMessage::ActionMessage result;
    result.action = Action::readFrom(stream);
    return result;
}
void ClientMessage::ActionMessage::writeTo(OutputStream& stream) const {
    stream.write(TAG);
    action.writeTo(stream);
}

const int ClientMessage::DebugUpdateDone::TAG = 2;
ClientMessage::DebugUpdateDone::DebugUpdateDone() = default;
ClientMessage::DebugUpdateDone ClientMessage::DebugUpdateDone::readFrom(InputStream& stream) {
    ClientMessage::DebugUpdateDone result;
    return result;
}
void ClientMessage::DebugUpdateDone::writeTo(OutputStream& stream) const {
    stream.write(TAG);
}

const int ClientMessage::RequestDebugState::TAG = 3;
ClientMessage::RequestDebugState::RequestDebugState() = default;
ClientMessage::RequestDebugState ClientMessage::RequestDebugState::readFrom(InputStream& stream) {
    ClientMessage::RequestDebugState result;
    return result;
}
void ClientMessage::RequestDebugState::writeTo(OutputStream& stream) const {
    stream.write(TAG);
}
