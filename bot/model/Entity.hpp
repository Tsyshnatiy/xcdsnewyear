#ifndef _MODEL_ENTITY_HPP_
#define _MODEL_ENTITY_HPP_

#include "CellIndex.hpp"
#include "EntityType.hpp"
#include "Id.hpp"
#include <memory>

class Entity {
public:
    EntityID id;
    int health;
    std::shared_ptr<PlayerID> playerId;
    CellIndex position;
    EntityType entityType;
    bool active;
    Entity() = default;
    Entity(EntityID id, std::shared_ptr<PlayerID> playerId, EntityType entityType, CellIndex position, int health, bool active);
    static Entity readFrom(class InputStream& stream);
    void writeTo(class OutputStream& stream) const;
};

#endif
