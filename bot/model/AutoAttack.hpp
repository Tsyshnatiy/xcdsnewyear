#ifndef _MODEL_AUTO_ATTACK_HPP_
#define _MODEL_AUTO_ATTACK_HPP_

#include "EntityType.hpp"
#include <vector>

class AutoAttack {
public:
    size_t pathfindRange;
    std::vector<EntityType> validTargets;
    AutoAttack();
    AutoAttack(size_t pathfindRange, std::vector<EntityType> validTargets);
    static AutoAttack readFrom(class InputStream& stream);
    void writeTo(class OutputStream& stream) const;
};

#endif
