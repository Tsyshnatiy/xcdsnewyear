#include "DebugData.hpp"
#include "../Enum.hpp"
#include "../Stream.hpp"
#include <array>

static constexpr std::array<std::shared_ptr<DebugData>(*)(InputStream& stream), 3> makeData{
    [](InputStream& stream) {
        return std::shared_ptr<DebugData>(new auto(DebugData::Log::readFrom(stream)));
    },
    [](InputStream& stream) {
        return std::shared_ptr<DebugData>(new auto(DebugData::Primitives::readFrom(stream)));
    },
    [](InputStream& stream) {
        return std::shared_ptr<DebugData>(new auto(DebugData::PlacedText::readFrom(stream)));
    },
};

std::shared_ptr<DebugData> DebugData::readFrom(InputStream& stream) {
    return makeData.at(stream.readInt())(stream);
}

const int DebugData::Log::TAG = 0;
DebugData::Log::Log() = default;
DebugData::Log::Log(std::string text) : text(std::move(text)) { }
DebugData::Log DebugData::Log::readFrom(InputStream& stream) {
    return stream.readString();
}
void DebugData::Log::writeTo(OutputStream& stream) const {
    stream.write(TAG);
    stream.write(text);
}

const int DebugData::Primitives::TAG = 1;
DebugData::Primitives::Primitives() = default;
DebugData::Primitives::Primitives(std::vector<ColoredVertex> vertices, PrimitiveType primitiveType) : vertices(std::move(vertices)), primitiveType(primitiveType) { }
DebugData::Primitives DebugData::Primitives::readFrom(InputStream& stream) {
    DebugData::Primitives result;
    size_t verticesCount = stream.readInt();
    result.vertices.reserve(stream.readInt());
    while (verticesCount--) {
        result.vertices.push_back(ColoredVertex::readFrom(stream));
    }
    result.primitiveType = to<PrimitiveType>(stream.readInt());
    return result;
}
void DebugData::Primitives::writeTo(OutputStream& stream) const {
    stream.write(TAG);
    stream.write((int)(vertices.size()));
    for (const ColoredVertex& verticesElement : vertices) {
        verticesElement.writeTo(stream);
    }
    stream.write((int)(primitiveType));
}

const int DebugData::PlacedText::TAG = 2;
DebugData::PlacedText::PlacedText() = default;
DebugData::PlacedText::PlacedText(ColoredVertex vertex, std::string text, float alignment, float size) : vertex(std::move(vertex)), text(std::move(text)), alignment(alignment), size(size) { }
DebugData::PlacedText DebugData::PlacedText::readFrom(InputStream& stream) {
    DebugData::PlacedText result;
    result.vertex = ColoredVertex::readFrom(stream);
    result.text = stream.readString();
    result.alignment = stream.readFloat();
    result.size = stream.readFloat();
    return result;
}
void DebugData::PlacedText::writeTo(OutputStream& stream) const {
    stream.write(TAG);
    vertex.writeTo(stream);
    stream.write(text);
    stream.write(alignment);
    stream.write(size);
}
