#ifndef _MODEL_ENTITY_ACTION_HPP_
#define _MODEL_ENTITY_ACTION_HPP_

#include <memory>

class AttackAction;
class BuildAction;
class MoveAction;
class RepairAction;

class EntityAction {
public:
    std::shared_ptr<MoveAction> moveAction;
    std::shared_ptr<BuildAction> buildAction;
    std::shared_ptr<AttackAction> attackAction;
    std::shared_ptr<RepairAction> repairAction;
    EntityAction();
    EntityAction(std::shared_ptr<MoveAction> moveAction, std::shared_ptr<BuildAction> buildAction, std::shared_ptr<AttackAction> attackAction, std::shared_ptr<RepairAction> repairAction);
    static EntityAction readFrom(class InputStream& stream);
    void writeTo(class OutputStream& stream) const;
};

#endif
