#ifndef _MODEL_BUILD_ACTION_HPP_
#define _MODEL_BUILD_ACTION_HPP_

#include "EntityType.hpp"
#include "CellIndex.hpp"

class BuildAction {
public:
    EntityType entityType;
    CellIndex position;
    BuildAction() = default;
    BuildAction(EntityType entityType, CellIndex position);
    static BuildAction readFrom(class InputStream& stream);
    void writeTo(class OutputStream& stream) const;
    bool operator ==(const BuildAction& other) const;
};
namespace std {
    template<>
    struct hash<BuildAction> {
        size_t operator ()(const BuildAction& value) const;
    };
}

#endif
