#ifndef _MODEL_DEBUG_DATA_HPP_
#define _MODEL_DEBUG_DATA_HPP_

#include "ColoredVertex.hpp"
#include "PrimitiveType.hpp"
#include <string>
#include <vector>

class DebugData {
public:
    class Log;
    class Primitives;
    class PlacedText;

    static std::shared_ptr<DebugData> readFrom(class InputStream& stream);
    virtual void writeTo(class OutputStream& stream) const = 0;
};

class DebugData::Log : public DebugData {
public:
    static const int TAG;
public:
    std::string text;
    Log();
    Log(std::string text);
    static Log readFrom(InputStream& stream);
    void writeTo(OutputStream& stream) const override;
};

class DebugData::Primitives : public DebugData {
public:
    static const int TAG;
public:
    std::vector<ColoredVertex> vertices;
    PrimitiveType primitiveType;
    Primitives();
    Primitives(std::vector<ColoredVertex> vertices, PrimitiveType primitiveType);
    static Primitives readFrom(InputStream& stream);
    void writeTo(OutputStream& stream) const override;
};

class DebugData::PlacedText : public DebugData {
public:
    static const int TAG;
public:
    ColoredVertex vertex;
    std::string text;
    float alignment;
    float size;
    PlacedText();
    PlacedText(ColoredVertex vertex, std::string text, float alignment, float size);
    static PlacedText readFrom(InputStream& stream);
    void writeTo(OutputStream& stream) const override;
};

#endif
