#include "DebugCommand.hpp"
#include "../Stream.hpp"
#include "DebugData.hpp"
#include <array>

static constexpr std::array<std::shared_ptr<DebugCommand>(*)(InputStream& stream), 4> makeCommand{
    [](InputStream& stream) {
        return std::shared_ptr<DebugCommand>(new auto(DebugCommand::Add::readFrom(stream)));
    },
    [](InputStream& stream) {
        return std::shared_ptr<DebugCommand>(new auto(DebugCommand::Clear::readFrom(stream)));
    },
    [](InputStream& stream) {
        return std::shared_ptr<DebugCommand>(new auto(DebugCommand::SetAutoFlush::readFrom(stream)));
    },
    [](InputStream& stream) {
        return std::shared_ptr<DebugCommand>(new auto(DebugCommand::Flush::readFrom(stream)));
    },
};

std::shared_ptr<DebugCommand> DebugCommand::readFrom(InputStream& stream) {
    return makeCommand.at(stream.readInt())(stream);
}

const int DebugCommand::Add::TAG = 0;
DebugCommand::Add::Add() = default;
DebugCommand::Add::Add(std::shared_ptr<DebugData> data) : data(std::move(data)) { }
DebugCommand::Add DebugCommand::Add::readFrom(InputStream& stream) {
    DebugCommand::Add result;
    result.data = DebugData::readFrom(stream);
    return result;
}
void DebugCommand::Add::writeTo(OutputStream& stream) const {
    stream.write(TAG);
    data->writeTo(stream);
}

const int DebugCommand::Clear::TAG = 1;
DebugCommand::Clear::Clear() = default;
DebugCommand::Clear DebugCommand::Clear::readFrom(InputStream& stream) {
    DebugCommand::Clear result;
    return result;
}
void DebugCommand::Clear::writeTo(OutputStream& stream) const {
    stream.write(TAG);
}

const int DebugCommand::SetAutoFlush::TAG = 2;
DebugCommand::SetAutoFlush::SetAutoFlush() = default;
DebugCommand::SetAutoFlush::SetAutoFlush(bool enable) : enable(enable) { }
DebugCommand::SetAutoFlush DebugCommand::SetAutoFlush::readFrom(InputStream& stream) {
    DebugCommand::SetAutoFlush result;
    result.enable = stream.readBool();
    return result;
}
void DebugCommand::SetAutoFlush::writeTo(OutputStream& stream) const {
    stream.write(TAG);
    stream.write(enable);
}

const int DebugCommand::Flush::TAG = 3;
DebugCommand::Flush::Flush() = default;
DebugCommand::Flush DebugCommand::Flush::readFrom(InputStream& stream) {
    DebugCommand::Flush result;
    return result;
}
void DebugCommand::Flush::writeTo(OutputStream& stream) const {
    stream.write(TAG);
}
