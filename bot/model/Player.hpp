#ifndef _MODEL_PLAYER_HPP_
#define _MODEL_PLAYER_HPP_

#include "Id.hpp"
#include <functional>

class Player {
public:
    PlayerID id;
    int score;
    int resource;
    Player();
    Player(PlayerID id, int score, int resource);
    static Player readFrom(class InputStream& stream);
    void writeTo(class OutputStream& stream) const;
    bool operator ==(const Player& other) const;
};
namespace std {
    template<>
    struct hash<Player> {
        size_t operator ()(const Player& value) const;
    };
}

#endif
