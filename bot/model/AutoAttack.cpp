#include "AutoAttack.hpp"
#include "../Enum.hpp"
#include "../Stream.hpp"

AutoAttack::AutoAttack() = default;
AutoAttack::AutoAttack(size_t pathfindRange, std::vector<EntityType> validTargets) : pathfindRange(pathfindRange), validTargets(std::move(validTargets)) { }
AutoAttack AutoAttack::readFrom(InputStream& stream) {
    AutoAttack result{static_cast<size_t>(stream.readInt()), std::vector<EntityType>(stream.readInt())};
    for (EntityType& validTarget : result.validTargets) {
        validTarget = to<EntityType>(stream.readInt());
    }
    return result;
}
void AutoAttack::writeTo(OutputStream& stream) const {
    stream.write((int)(pathfindRange));
    stream.write((int)(validTargets.size()));
    for (const EntityType& validTargetsElement : validTargets) {
        stream.write((int)(validTargetsElement));
    }
}
