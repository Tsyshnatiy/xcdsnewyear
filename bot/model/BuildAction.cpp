#include "BuildAction.hpp"
#include "../Enum.hpp"
#include "../Stream.hpp"
#include "Hashing.hpp"

BuildAction::BuildAction(EntityType entityType, CellIndex position) : entityType(entityType), position(position) { }

BuildAction BuildAction::readFrom(InputStream& stream) {
    BuildAction result;
    result.entityType = to<EntityType>(stream.readInt());
    result.position = CellIndex::readFrom(stream);
    return result;
}
void BuildAction::writeTo(OutputStream& stream) const {
    stream.write((int)(entityType));
    position.writeTo(stream);
}
bool BuildAction::operator ==(const BuildAction& other) const {
    return entityType == other.entityType && position == other.position;
}
size_t std::hash<BuildAction>::operator ()(const BuildAction& value) const {
    return CombinedHash{} << value.entityType << value.position;
}
