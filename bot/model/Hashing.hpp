#ifndef _MODEL_HASHING_HPP_
#define _MODEL_HASHING_HPP_

#include <functional>

class CombinedHash
{
public:
    explicit CombinedHash(const size_t initial = 0) noexcept : combo(initial) {}

    template <typename Numeric>
    CombinedHash& operator<<(const Numeric value) noexcept
    {
        combo ^= std::hash<Numeric>{}(value)+0x9e3779b9 + (combo << 6) + (combo >> 2);
        return *this;
    }

    operator size_t() const noexcept
    {
        return combo;
    }

private:
    size_t combo;
};

#endif
