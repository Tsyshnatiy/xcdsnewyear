#include "AttackAction.hpp"
#include "../Stream.hpp"
#include "../Utility.hpp"
#include "AutoAttack.hpp"

AttackAction::AttackAction() = default;
AttackAction::AttackAction(std::shared_ptr<int> target, std::shared_ptr<AutoAttack> autoAttack) : target(std::move(target)), autoAttack(std::move(autoAttack)) { }
AttackAction AttackAction::readFrom(InputStream& stream) {
    AttackAction result;
    auto* const target = stream.readBool() ? new auto(stream.readInt()) : nullptr;
    result.target.reset(target);

    auto* const autoAttack = stream.readBool() ? new auto(AutoAttack::readFrom(stream)) : nullptr;
    result.autoAttack.reset(autoAttack);
    return result;
}
void AttackAction::writeTo(OutputStream& stream) const {
    stream.write(!!target);
    if (target) {
        stream.write(*target);
    }
    stream.write(!!autoAttack);
    do_if (autoAttack)->writeTo(stream);
}
