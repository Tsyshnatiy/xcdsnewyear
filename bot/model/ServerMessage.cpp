#include "ServerMessage.hpp"
#include "../Stream.hpp"
#include "Entity.hpp"
#include "EntityProperties.hpp"
#include "Player.hpp"
#include <array>

static constexpr std::array<std::shared_ptr<ServerMessage>(*)(InputStream& stream), 3> makeMessage{
    [](InputStream& stream) {
        return std::shared_ptr<ServerMessage>(new auto(ServerMessage::GetAction::readFrom(stream)));
    },
    [](InputStream& stream) {
        return std::shared_ptr<ServerMessage>(new auto(ServerMessage::Finish::readFrom(stream)));
    },
    [](InputStream& stream) {
        return std::shared_ptr<ServerMessage>(new auto(ServerMessage::DebugUpdate::readFrom(stream)));
    },
};

std::shared_ptr<ServerMessage> ServerMessage::readFrom(InputStream& stream) {
    return makeMessage.at(stream.readInt())(stream);
}

ServerMessage::GetAction::GetAction() = default;
ServerMessage::GetAction::GetAction(PlayerView playerView, bool debugAvailable) : playerView(std::move(playerView)), debugAvailable(debugAvailable) { }
ServerMessage::GetAction ServerMessage::GetAction::readFrom(InputStream& stream) {
    ServerMessage::GetAction result;
    result.playerView = PlayerView::readFrom(stream);
    result.debugAvailable = stream.readBool();
    return result;
}
void ServerMessage::GetAction::writeTo(OutputStream& stream) const {
    stream.write(TAG);
    playerView.writeTo(stream);
    stream.write(debugAvailable);
}

ServerMessage::Finish::Finish() = default;
ServerMessage::Finish ServerMessage::Finish::readFrom(InputStream& stream) {
    ServerMessage::Finish result;
    return result;
}
void ServerMessage::Finish::writeTo(OutputStream& stream) const {
    stream.write(TAG);
}

ServerMessage::DebugUpdate::DebugUpdate() = default;
ServerMessage::DebugUpdate::DebugUpdate(PlayerView playerView) : playerView(std::move(playerView)) { }
ServerMessage::DebugUpdate ServerMessage::DebugUpdate::readFrom(InputStream& stream) {
    ServerMessage::DebugUpdate result;
    result.playerView = PlayerView::readFrom(stream);
    return result;
}
void ServerMessage::DebugUpdate::writeTo(OutputStream& stream) const {
    stream.write(TAG);
    playerView.writeTo(stream);
}
