#ifndef _MODEL_CAMERA_HPP_
#define _MODEL_CAMERA_HPP_

#include "Vec2Float.hpp"

class Camera {
public:
    Vec2Float center;
    float rotation;
    float attack;
    float distance;
    bool perspective;
    Camera();
    Camera(Vec2Float center, float rotation, float attack, float distance, bool perspective);
    static Camera readFrom(class InputStream& stream);
    void writeTo(class OutputStream& stream) const;
};

#endif
