#ifndef _MODEL_PLAYER_VIEW_HPP_
#define _MODEL_PLAYER_VIEW_HPP_

#include "EntityType.hpp"
#include "Player.hpp"
#include "CellIndex.hpp"
#include "Entity.hpp"
#include "EntityProperties.hpp"

#include <optional>
#include <unordered_set>
#include <unordered_map>

class PlayerView {
public:
    PlayerID myId;
    bool fogOfWar;
    std::size_t mapSize;
    int maxTickCount;
    int maxPathfindNodes;
    int currentTick;

    Player player1;
    Player player2;

    std::unordered_map<EntityID, Entity> firstPlayerEntities;
    std::unordered_map<EntityID, Entity> secondPlayerEntities;
    std::unordered_map<EntityID, Entity> resources;

    std::unordered_map<EntityType, EntityProperties> entityProperties;

    std::unordered_map<CellIndex, EntityID> entityPositions;

    const Player& getMyPlayer() const;
    const Player& getEnemyPlayer() const;
    
    std::unordered_set<EntityID> getBuildingIds(std::optional<PlayerID> playerId) const;

    const Entity* getEntityAtCell(CellIndex cellIndex) const;
    Entity* getEntityAtCell(CellIndex cellIndex);

    const Entity* getEntityById(EntityID id) const;
    Entity* getEntityById(EntityID id);

    std::unordered_set<EntityID> getEntityIds(PlayerID playerId, EntityType type) const;

    std::unordered_map<EntityID, Entity>& getEntities(std::optional<PlayerID> playerId);
    const std::unordered_map<EntityID, Entity>& getEntities(std::optional<PlayerID> playerId) const;

    bool isUnitDead(EntityID id) const;

    static PlayerView readFrom(class InputStream& stream);
    void writeTo(class OutputStream& stream) const;
};

#endif
