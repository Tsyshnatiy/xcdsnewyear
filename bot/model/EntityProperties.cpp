#include "EntityProperties.hpp"
#include "../Stream.hpp"
#include "../Utility.hpp"

EntityProperties::EntityProperties() = default;
EntityProperties::EntityProperties(int size, int buildScore, int destroyScore, bool canMove, int populationProvide, int populationUse, int maxHealth, int initialCost, int sightRange, int resourcePerHealth, std::shared_ptr<BuildProperties> build, std::shared_ptr<AttackProperties> attack, std::shared_ptr<RepairProperties> repair) : size(size), buildScore(buildScore), destroyScore(destroyScore), canMove(canMove), populationProvide(populationProvide), populationUse(populationUse), maxHealth(maxHealth), initialCost(initialCost), sightRange(sightRange), resourcePerHealth(resourcePerHealth), build(build), attack(attack), repair(repair) { }
EntityProperties EntityProperties::readFrom(InputStream& stream) {
    EntityProperties result;
    result.size = stream.readInt();
    result.buildScore = stream.readInt();
    result.destroyScore = stream.readInt();
    result.canMove = stream.readBool();
    result.populationProvide = stream.readInt();
    result.populationUse = stream.readInt();
    result.maxHealth = stream.readInt();
    result.initialCost = stream.readInt();
    result.sightRange = stream.readInt();
    result.resourcePerHealth = stream.readInt();

    auto* const build = stream.readBool() ? new auto(BuildProperties::readFrom(stream)) : nullptr;
    result.build.reset(build);
    auto* const attack = stream.readBool() ? new auto(AttackProperties::readFrom(stream)) : nullptr;
    result.attack.reset(attack);
    auto* const repair = stream.readBool() ? new auto(RepairProperties::readFrom(stream)) : nullptr;
    result.repair.reset(repair);
    return result;
}
void EntityProperties::writeTo(OutputStream& stream) const {
    stream.write(size);
    stream.write(buildScore);
    stream.write(destroyScore);
    stream.write(canMove);
    stream.write(populationProvide);
    stream.write(populationUse);
    stream.write(maxHealth);
    stream.write(initialCost);
    stream.write(sightRange);
    stream.write(resourcePerHealth);
    stream.write(!!build);
    do_if (build)->writeTo(stream);
    stream.write(!!attack);
    do_if (attack)->writeTo(stream);
    stream.write(!!repair);
    do_if (repair)->writeTo(stream);
}
