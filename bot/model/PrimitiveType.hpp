#ifndef _MODEL_PRIMITIVE_TYPE_HPP_
#define _MODEL_PRIMITIVE_TYPE_HPP_

enum PrimitiveType : char8_t {
    LINES = 0,
    TRIANGLES = 1,

    EnumFirst = LINES,
    EnumLast = TRIANGLES,
};

#endif
