#include "RepairProperties.hpp"
#include "../Enum.hpp"
#include "../Stream.hpp"

RepairProperties::RepairProperties() = default;
RepairProperties::RepairProperties(std::vector<EntityType> validTargets, int power) : validTargets(std::move(validTargets)), power(power) { }
RepairProperties RepairProperties::readFrom(InputStream& stream) {
    RepairProperties result;
    size_t validTargetsCount = stream.readInt();
    result.validTargets.reserve(validTargetsCount);
    while (validTargetsCount--) {
        result.validTargets.push_back(to<EntityType>(stream.readInt()));
    }
    result.power = stream.readInt();
    return result;
}
void RepairProperties::writeTo(OutputStream& stream) const {
    stream.write((int)(validTargets.size()));
    for (const EntityType& validTargetsElement : validTargets) {
        stream.write((int)(validTargetsElement));
    }
    stream.write(power);
}
