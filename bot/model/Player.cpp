#include "Player.hpp"
#include "../Stream.hpp"
#include "Hashing.hpp"

Player::Player() = default;
Player::Player(PlayerID id, int score, int resource) : id(id), score(score), resource(resource) { }
Player Player::readFrom(InputStream& stream) {
    Player result;
    result.id = stream.readInt();
    result.score = stream.readInt();
    result.resource = stream.readInt();
    return result;
}
void Player::writeTo(OutputStream& stream) const {
    stream.write(id);
    stream.write(score);
    stream.write(resource);
}
bool Player::operator ==(const Player& other) const {
    return id == other.id && score == other.score && resource == other.resource;
}
size_t std::hash<Player>::operator ()(const Player& value) const {
    return CombinedHash{} << value.id << value.score << value.resource;
}
