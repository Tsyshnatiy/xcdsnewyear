#include "Vec2Int.hpp"
#include "../Stream.hpp"
#include "Hashing.hpp"

Vec2Int::Vec2Int() = default;
Vec2Int::Vec2Int(int x, int y) : x(x), y(y) { }
Vec2Int Vec2Int::readFrom(InputStream& stream) {
    Vec2Int result;
    result.x = stream.readInt();
    result.y = stream.readInt();
    return result;
}
void Vec2Int::writeTo(OutputStream& stream) const {
    stream.write(x);
    stream.write(y);
}
bool Vec2Int::operator ==(const Vec2Int& other) const {
    return x == other.x && y == other.y;
}
size_t std::hash<Vec2Int>::operator ()(const Vec2Int& value) const {
    return CombinedHash{} << value.x << value.y;
}
