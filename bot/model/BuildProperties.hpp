#ifndef _MODEL_BUILD_PROPERTIES_HPP_
#define _MODEL_BUILD_PROPERTIES_HPP_

#include "EntityType.hpp"
#include <memory>
#include <vector>

class BuildProperties {
public:
    std::vector<EntityType> options;
    std::shared_ptr<int> initHealth;
    BuildProperties();
    BuildProperties(std::vector<EntityType> options, std::shared_ptr<int> initHealth);
    static BuildProperties readFrom(class InputStream& stream);
    void writeTo(class OutputStream& stream) const;
};

#endif
