#ifndef _CELL_INDEX_HPP_
#define _CELL_INDEX_HPP_

#include <compare>
#include <cstddef>
#include <functional> // for std::hash

struct CellIndex final
{
    std::size_t x = 0;
    std::size_t y = 0;

    std::strong_ordering operator <=> (const CellIndex& other) const = default;

    static CellIndex readFrom(class InputStream& stream);
    void writeTo(class OutputStream& stream) const;
};

namespace std {
    template<>
    struct hash<CellIndex> {
        size_t operator ()(const CellIndex& value) const;
    };
}

#endif
