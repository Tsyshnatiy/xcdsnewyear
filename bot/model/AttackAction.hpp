#ifndef _MODEL_ATTACK_ACTION_HPP_
#define _MODEL_ATTACK_ACTION_HPP_

#include <memory>

class AutoAttack;

class AttackAction {
public:
    std::shared_ptr<int> target;
    std::shared_ptr<AutoAttack> autoAttack;
    AttackAction();
    AttackAction(std::shared_ptr<int> target, std::shared_ptr<AutoAttack> autoAttack);
    static AttackAction readFrom(class InputStream& stream);
    void writeTo(class OutputStream& stream) const;
};

#endif
