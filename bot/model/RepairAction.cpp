#include "RepairAction.hpp"
#include "../Stream.hpp"
#include "Hashing.hpp"

RepairAction::RepairAction() = default;
RepairAction::RepairAction(int target) : target(target) { }
RepairAction RepairAction::readFrom(InputStream& stream) {
    RepairAction result;
    result.target = stream.readInt();
    return result;
}
void RepairAction::writeTo(OutputStream& stream) const {
    stream.write(target);
}
bool RepairAction::operator ==(const RepairAction& other) const {
    return target == other.target;
}
size_t std::hash<RepairAction>::operator ()(const RepairAction& value) const {
    return CombinedHash{} << value.target;
}
