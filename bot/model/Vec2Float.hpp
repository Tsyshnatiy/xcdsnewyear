#ifndef _MODEL_VEC2_FLOAT_HPP_
#define _MODEL_VEC2_FLOAT_HPP_

class Vec2Float {
public:
    float x;
    float y;
    Vec2Float();
    Vec2Float(float x, float y);
    static Vec2Float readFrom(class InputStream& stream);
    void writeTo(class OutputStream& stream) const;
};

#endif
