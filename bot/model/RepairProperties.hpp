#ifndef _MODEL_REPAIR_PROPERTIES_HPP_
#define _MODEL_REPAIR_PROPERTIES_HPP_

#include "EntityType.hpp"
#include <vector>

class RepairProperties {
public:
    std::vector<EntityType> validTargets;
    int power;
    RepairProperties();
    RepairProperties(std::vector<EntityType> validTargets, int power);
    static RepairProperties readFrom(class InputStream& stream);
    void writeTo(class OutputStream& stream) const;
};

#endif
