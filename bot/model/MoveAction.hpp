#ifndef _MODEL_MOVE_ACTION_HPP_
#define _MODEL_MOVE_ACTION_HPP_

#include "CellIndex.hpp"

class MoveAction {
public:
    CellIndex target;
    bool findClosestPosition;
    bool breakThrough;

    MoveAction() = default;
    MoveAction(CellIndex target, bool findClosestPosition, bool breakThrough);

    static MoveAction readFrom(class InputStream& stream);
    void writeTo(class OutputStream& stream) const;
    
    bool operator ==(const MoveAction& other) const;
};
namespace std {
    template<>
    struct hash<MoveAction> {
        size_t operator ()(const MoveAction& value) const;
    };
}

#endif
