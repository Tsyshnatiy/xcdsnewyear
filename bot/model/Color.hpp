#ifndef _MODEL_COLOR_HPP_
#define _MODEL_COLOR_HPP_

class Color {
public:
    float r;
    float g;
    float b;
    float a;
    Color();
    Color(float r, float g, float b, float a);
    static Color readFrom(class InputStream& stream);
    void writeTo(class OutputStream& stream) const;
};

#endif
