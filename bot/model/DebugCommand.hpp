#ifndef _MODEL_DEBUG_COMMAND_HPP_
#define _MODEL_DEBUG_COMMAND_HPP_

#include <memory>

class DebugData;

class DebugCommand {
public:
    class Add;
    class Clear;
    class SetAutoFlush;
    class Flush;

    static std::shared_ptr<DebugCommand> readFrom(class InputStream& stream);
    virtual void writeTo(class OutputStream& stream) const = 0;
};

class DebugCommand::Add : public DebugCommand {
public:
    static const int TAG;
public:
    std::shared_ptr<DebugData> data;
    Add();
    Add(std::shared_ptr<DebugData> data);
    static Add readFrom(InputStream& stream);
    void writeTo(OutputStream& stream) const override;
};

class DebugCommand::Clear : public DebugCommand {
public:
    static const int TAG;
public:
    Clear();
    static Clear readFrom(InputStream& stream);
    void writeTo(OutputStream& stream) const override;
};

class DebugCommand::SetAutoFlush : public DebugCommand {
public:
    static const int TAG;
public:
    bool enable;
    SetAutoFlush();
    SetAutoFlush(bool enable);
    static SetAutoFlush readFrom(InputStream& stream);
    void writeTo(OutputStream& stream) const override;
};

class DebugCommand::Flush : public DebugCommand {
public:
    static const int TAG;
public:
    Flush();
    static Flush readFrom(InputStream& stream);
    void writeTo(OutputStream& stream) const override;
};

#endif
