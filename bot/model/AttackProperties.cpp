#include "AttackProperties.hpp"
#include "../Stream.hpp"
#include "Hashing.hpp"

AttackProperties::AttackProperties() = default;
AttackProperties::AttackProperties(int attackRange, int damage, bool collectResource) : attackRange(attackRange), damage(damage), collectResource(collectResource) { }
AttackProperties AttackProperties::readFrom(InputStream& stream) {
    AttackProperties result;
    result.attackRange = stream.readInt();
    result.damage = stream.readInt();
    result.collectResource = stream.readBool();
    return result;
}
void AttackProperties::writeTo(OutputStream& stream) const {
    stream.write(attackRange);
    stream.write(damage);
    stream.write(collectResource);
}
bool AttackProperties::operator ==(const AttackProperties& other) const {
    return attackRange == other.attackRange && damage == other.damage && collectResource == other.collectResource;
}
size_t std::hash<AttackProperties>::operator ()(const AttackProperties& value) const {
    return CombinedHash{} << value.attackRange << value.damage << value.collectResource;
}
