#include "BuildProperties.hpp"
#include "../Enum.hpp"
#include "../Stream.hpp"

BuildProperties::BuildProperties() = default;
BuildProperties::BuildProperties(std::vector<EntityType> options, std::shared_ptr<int> initHealth) : options(std::move(options)), initHealth(std::move(initHealth)) { }
BuildProperties BuildProperties::readFrom(InputStream& stream) {
    BuildProperties result;
    auto& options = result.options;

    size_t optionsSize = stream.readInt();
    options.reserve(optionsSize);
    while (optionsSize--) {
        options.push_back(to<EntityType>(stream.readInt()));
    }

    auto* const initHealth = stream.readBool() ? new auto(stream.readInt()) : nullptr;
    result.initHealth.reset(initHealth);
    return result;
}
void BuildProperties::writeTo(OutputStream& stream) const {
    stream.write((int)(options.size()));
    for (const EntityType& optionsElement : options) {
        stream.write((int)(optionsElement));
    }
    stream.write(!!initHealth);
    if (initHealth) {
        stream.write(*initHealth);
    }
}
