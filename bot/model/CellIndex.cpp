#include "CellIndex.hpp"
#include "../Stream.hpp"
#include "Hashing.hpp"
#include <cassert>

CellIndex CellIndex::readFrom(InputStream& stream) {
    CellIndex result;
    const auto x = stream.readInt();
    const auto y = stream.readInt();

    assert(x >= 0 && y >= 0);

    result.x = static_cast<std::size_t>(x);
    result.y = static_cast<std::size_t>(y);

    return result;
}
void CellIndex::writeTo(OutputStream& stream) const
{
    const auto xx = static_cast<int>(x);
    const auto yy = static_cast<int>(y);
    stream.write(xx);
    stream.write(yy);
}
size_t std::hash<CellIndex>::operator ()(const CellIndex& value) const
{
    return CombinedHash{} << value.x << value.y;
}