#include "MyStrategy.hpp"
#include "DebugInterface.hpp"
#include "PointsOfInterest.hpp"

#include "model/Action.hpp"
#include "model/AttackAction.hpp"
#include "model/AutoAttack.hpp"
#include "model/BuildAction.hpp"
#include "model/DebugCommand.hpp"
#include "model/DebugState.hpp"
#include "model/Entity.hpp"
#include "model/EntityAction.hpp"
#include "model/EntityProperties.hpp"
#include "model/MoveAction.hpp"
#include "model/PlayerView.hpp"

#include "economics/Production.hpp"
#include "economics/Harvester.hpp"
#include "economics/Builder.hpp"
#include "economics/PopulationComputer.hpp"
#include "economics/Population.hpp"

#include "military/AllOutMarshal.hpp"

#include <algorithm>
#include <memory>

MyStrategy::MyStrategy()
    : m_poi{},
      m_actionsCache{},
      m_treasury{},
      m_production{new Production{m_poi}},
      m_harvester(new Harvester{m_actionsCache}),
      m_builder(new Builder{m_actionsCache}),
      m_marshal(new AllOutMarshal{ m_poi })
{

}

MyStrategy::~MyStrategy() = default;

void MyStrategy::invalidateActionsCache(const PlayerView& playerView)
{
    const auto allMyEntities = playerView.getEntities(playerView.myId);

    { // Forget actions of dead units
        std::vector<EntityID> killedEntities;
        for (const auto& entry : m_actionsCache.entityActions)
        {
            const auto& entityId = entry.first;

            if (!allMyEntities.contains(entityId))
            {
                killedEntities.push_back(entityId);
            }
        }

        for (const auto killedEntityId : killedEntities)
        {
            m_actionsCache.entityActions.erase(killedEntityId);
        }
    }

    // Assign empty action to new units
    for (const auto& entry : allMyEntities)
    {
        const auto& entityId = entry.first;
        m_actionsCache.entityActions.try_emplace(entityId, EntityAction{});
    }
}

void MyStrategy::copyActionsToCache(const Action& tickActions)
{
    for (const auto& [id, action] : tickActions.entityActions)
    {
        m_actionsCache.entityActions.insert_or_assign(id, action);
    }
}

auto MyStrategy::getAutoAttackTargetsForCombatUnits() const
{
    std::vector<EntityType> autoAttackTargets;
    autoAttackTargets.reserve(EntityType::EnumLast - EntityType::EnumFirst);
    for (char8_t et = EntityType::EnumFirst ; et <= EntityType::EnumLast; ++et)
    {
        if (et != EntityType::RESOURCE)
        {
            autoAttackTargets.push_back(static_cast<EntityType>(et));
        }
    }
    return autoAttackTargets;
}

void MyStrategy::sendNewUnitsToRallyPoint(Action& tickActions, const PlayerView& playerView)
{
    const auto allMyUnits = playerView.getEntities(playerView.myId);

    for (const auto& [id, entity] : allMyUnits)
    {
        if (entity.entityType != EntityType::MELEE_UNIT &&
            entity.entityType != EntityType::RANGED_UNIT)
        {
            continue;
        }

        if (!entity.active || m_actionsCache.isBusy(id))
        {
            continue;
        }

        const auto rallyPoint = m_poi.getOrCreateRallyPoint(playerView);
        if (!rallyPoint)
        {
            return;
        }

        EntityAction newMeleeAction;
        auto autoAttack = std::make_shared<AutoAttack>(playerView.mapSize, getAutoAttackTargetsForCombatUnits());
        newMeleeAction.attackAction = std::make_shared<AttackAction>(nullptr, std::move(autoAttack));
        newMeleeAction.moveAction = std::make_shared<MoveAction>(rallyPoint.value(), true, true);
        tickActions.entityActions[id] = std::move(newMeleeAction);
    }
}

Action MyStrategy::getAction(const PlayerView& playerView, DebugInterface* const debugInterface)
{
    PopulationComputer populationComputer{playerView};
    const auto currentPopulation = populationComputer.compute();
    auto resourceAvailable = playerView.getMyPlayer().resource;

    invalidateActionsCache(playerView);

    Action result;

    // The order is crucial

    // Count money first
    m_treasury.onTick(playerView);

    // Send new slaves to harvest resources
    m_harvester->onTick(result, playerView);

    // Take harvesters and reserve some resources for buildings
    m_builder->onTick(result, currentPopulation, m_treasury, playerView);

    // Create units for the rest of resources if any
    m_production->onTick(result, playerView, m_treasury, currentPopulation);

    // Send new units to rally point
    sendNewUnitsToRallyPoint(result, playerView);

    // Ask general to command the army
    m_marshal->onTick(result, playerView);

    copyActionsToCache(result);

    return result;
}

void MyStrategy::debugUpdate(const PlayerView& playerView, DebugInterface& debugInterface)
{
    debugInterface.send(DebugCommand::Clear());
    debugInterface.getState();
}
