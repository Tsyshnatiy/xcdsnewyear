#ifndef _MAP_HELPERS_HPP_
#define _MAP_HELPERS_HPP_

#include "../model/CellIndex.hpp"

bool isOutOfMap(std::size_t mapW, std::size_t mapH, CellIndex cellIndex);

#endif
