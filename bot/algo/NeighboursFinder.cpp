#include "NeighboursFinder.hpp"

#include "MapHelpers.hpp"

NeighboursFinder::NeighboursFinder(std::size_t width, std::size_t height)
    : m_width{width},
      m_height{height}
{}

std::vector<CellIndex> NeighboursFinder::find(CellIndex index) const
{
    std::vector<CellIndex> result;

    const auto adder = [this, &result] (auto candidate)
    {
        if (!isOutOfMap(m_width, m_height, candidate))
        {
            result.push_back(std::move(candidate));
        }
    };

    adder(CellIndex{index.x + 1, index.y}); // right
    adder(CellIndex{index.x - 1, index.y}); // left
    adder(CellIndex{index.x, index.y + 1}); // top
    adder(CellIndex{index.x, index.y - 1}); // bottom

    return result;
}