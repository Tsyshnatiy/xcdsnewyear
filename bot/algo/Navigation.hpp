#ifndef _NAVIGATION_HPP_
#define _NAVIGATION_HPP_

#include "../model/EntityType.hpp"
#include "../model/Id.hpp"
#include "../model/Vec2Int.hpp"

#include "NeighboursFinder.hpp"

#include "../model/PlayerView.hpp"
#include "../model/Entity.hpp"

#include <unordered_set>
#include <queue>
#include <optional>

class PlayerView;

class Navigation
{
public:
    Navigation(const PlayerView& playerView);

    std::optional<CellIndex> getEmptyAreaAround(CellIndex origin, std::size_t size) const;

    template<typename Predicate>
    std::optional<EntityID> getClosestEntity(CellIndex origin, Predicate predicate) const
    {
        std::queue<CellIndex> openList;
        std::unordered_set<CellIndex> closedList;

        openList.push(origin);

        NeighboursFinder neightbourFinder{m_playerView.mapSize, m_playerView.mapSize};

        while(!openList.empty())
        {
            const auto currentOrigin = openList.front();
            openList.pop();
            // Cells inside openList are not unique, so just roll the cycle in this case
            if (closedList.contains(currentOrigin))
            {
                continue;
            }

            closedList.insert(currentOrigin);

            // Add cell's neighbours to the queue and closed list
            auto neighbours = neightbourFinder.find(currentOrigin);
            for (auto&& neighbour : neighbours)
            {
                if (closedList.contains(neighbour))
                {
                    continue;
                }

                openList.push(std::move(neighbour));
            }

            // If Cell is empty then continue
            const auto it = m_playerView.entityPositions.find(currentOrigin);
            if (it == m_playerView.entityPositions.cend())
            {
                continue;
            }

            // There is an entity in the cell
            const auto entityId = it->second;
            if (predicate(entityId))
            {
                return entityId;
            }
        }

        return std::nullopt;
    }

    bool isCellInsideBuilding(CellIndex center, const std::unordered_set<EntityID>& buildings) const;
    
private:
    bool isFreeArea(CellIndex center,
                    std::size_t size,
                    const std::unordered_set<EntityID>& allBuildings) const;

private:
    const PlayerView& m_playerView;
};

#endif
