#include "MapHelpers.hpp"

bool isOutOfMap(std::size_t mapW, std::size_t mapH, CellIndex cellIndex)
{
    return mapW <= cellIndex.x || mapH <= cellIndex.y;
}
