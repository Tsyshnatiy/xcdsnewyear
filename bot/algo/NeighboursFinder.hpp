#ifndef _NEIGHTBOURS_FINDER_HPP_
#define _NEIGHTBOURS_FINDER_HPP_

#include "../model/CellIndex.hpp"

#include <cstddef>
#include <vector>

class NeighboursFinder final
{
public:
    NeighboursFinder(std::size_t width, std::size_t height);

    std::vector<CellIndex> find(CellIndex index) const;
    
private:
    std::size_t m_width;
    std::size_t m_height;
};

#endif
