#include "Navigation.hpp"

#include "../model/EntityProperties.hpp"

Navigation::Navigation(const PlayerView& playerView)
    : m_playerView{playerView}
{

}

bool Navigation::isCellInsideBuilding(CellIndex cell, const std::unordered_set<EntityID>& buildings) const
{
    for (const auto buildingId : buildings)
    {
        const auto* building = m_playerView.getEntityById(buildingId);
        if (!building)
        {
            continue;
        }

        const auto pos = building->position;
        const auto& props = m_playerView.entityProperties.at(building->entityType);
        const auto size = props.size;

        const auto left = pos.x;
        const auto top = pos.y;
        const auto right = pos.x + size;
        const auto down = pos.y + size;

        if (left <= cell.x && cell.x < right
            && top <= cell.y && cell.y < down)
        {
            return true;
        } 
    }

    return false;
}

bool Navigation::isFreeArea(CellIndex leftTop, std::size_t size,
                            const std::unordered_set<EntityID>& allBuildings) const
{
    const auto mapBorder = m_playerView.mapSize - 1;

    const auto left = leftTop.x;
    const auto right = std::min(leftTop.x + size, mapBorder);
    const auto up = leftTop.y;
    const auto down = std::min(leftTop.y + size, mapBorder);

    for (std::size_t x = left ; x < right ; ++x)
    {
        for (std::size_t y = up ; y < down ; ++y)
        {
            CellIndex curCell{x, y};
            if (isCellInsideBuilding(curCell, allBuildings))
            {
                return false;
            }

            // Check is non building cell entity is in the cell
            // All other entities have single cell size
            if (m_playerView.entityPositions.contains(curCell))
            {
                return false;
            }
        }
    }

    return true;
}

std::optional<CellIndex> Navigation::getEmptyAreaAround(CellIndex origin, std::size_t size) const
{
    std::queue<CellIndex> openList;
    std::unordered_set<CellIndex> closedList;

    openList.push(origin);

    NeighboursFinder neighbourFinder{m_playerView.mapSize, m_playerView.mapSize};
    const auto allBuildings = m_playerView.getBuildingIds(std::nullopt);

    while(!openList.empty())
    {
        const auto currentOrigin = openList.front();
        openList.pop();

        // Cells inside openList are not unique, so just roll the cycle in this case
        if (closedList.contains(currentOrigin))
        {
            continue;
        }

        closedList.insert(currentOrigin);

        // Add cell's neighbours to the queue and closed list
        auto neighbours = neighbourFinder.find(currentOrigin);
        for (auto neighbour : neighbours)
        {
            if (closedList.contains(neighbour))
            {
                continue;
            }

            openList.push(neighbour);
        }

        if (isFreeArea(currentOrigin, size, allBuildings))
        {
            return currentOrigin;
        }
    }

    return std::nullopt;

}
