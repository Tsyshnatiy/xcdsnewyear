#include "DebugInterface.hpp"
#include "MyStrategy.hpp"
#include "Stream.hpp"
#include "TcpStream.hpp"
#include "model/ClientMessage.hpp"
#include "model/EntityAction.hpp"
#include "model/ServerMessage.hpp"
#include <iostream>

class Runner {
public:
    Runner(const std::string_view host, const int port, const std::string_view token)
    {
        auto tcpStream = std::make_shared<TcpStream>(host, port);
        inputStream = getInputStream(tcpStream);
        outputStream = getOutputStream(std::move(tcpStream));

        outputStream->write(token);
        outputStream->flush();
    }
    void run()
    {
        DebugInterface debugInterface(inputStream, outputStream);
        MyStrategy myStrategy;
        while (true) {
            const auto message = ServerMessage::readFrom(*inputStream);
            if (const auto getActionMessage = std::dynamic_pointer_cast<ServerMessage::GetAction>(message)) {
                DebugInterface* const debug = getActionMessage->debugAvailable ? &debugInterface : nullptr;
                ClientMessage::ActionMessage(myStrategy.getAction(getActionMessage->playerView, debug)).writeTo(*outputStream);
            } else if (const auto debugUpdateMessage = std::dynamic_pointer_cast<ServerMessage::DebugUpdate>(message)) {
                myStrategy.debugUpdate(debugUpdateMessage->playerView, debugInterface);
                ClientMessage::DebugUpdateDone().writeTo(*outputStream);
            } else if (const auto finishMessage = std::dynamic_pointer_cast<ServerMessage::Finish>(message)) {
                break;
            }
            outputStream->flush();
        }
    }

private:
    std::shared_ptr<InputStream> inputStream;
    std::shared_ptr<OutputStream> outputStream;
};

int main(int argc, char* argv[])
{
    const std::string_view host = argc < 2 ? "127.0.0.1" : argv[1];
    const int port = argc < 3 ? 31001 : atoi(argv[2]);
    const std::string_view token = argc < 4 ? "0000000000000000" : argv[3];
    try
    {
        Runner(host, port, token).run();
    }
    catch (std::exception& e)
    {
        std::cout << e.what() << std::endl;
        return 1;
    }
    return 0;
}