#ifndef _GAME_PHASE_HPP_
#define _GAME_PHASE_HPP_

#include "model/Id.hpp"
#include "model/CellIndex.hpp"

#include <optional>

class PlayerView;

class GamePhase final
{
public:
    using Phase = std::size_t;

    Phase getPhase(const PlayerView& playerView) const;
};

#endif
