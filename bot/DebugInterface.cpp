#include "DebugInterface.hpp"
#include "Stream.hpp"
#include "model/ClientMessage.hpp"
#include "model/DebugCommand.hpp"
#include "model/DebugState.hpp"

DebugInterface::DebugInterface(std::shared_ptr<InputStream> inputStream, std::shared_ptr<OutputStream> outputStream)
    : inputStream(std::move(inputStream))
    , outputStream(std::move(outputStream))
{
}

void DebugInterface::send(const DebugCommand& command)
{
    // TODO: Construct actual message, this is a hack :)
    outputStream->write(ClientMessage::DebugMessage::TAG);
    command.writeTo(*outputStream);
    outputStream->flush();
}

DebugState DebugInterface::getState()
{
    ClientMessage::RequestDebugState().writeTo(*outputStream);
    outputStream->flush();
    return DebugState::readFrom(*inputStream);
}