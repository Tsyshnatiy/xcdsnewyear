#ifndef _MODEL_UTILITY_HPP_
#define _MODEL_UTILITY_HPP_

#include <type_traits>

#define const_this const_cast<const std::remove_pointer_t<decltype(this)>*>(this)
#define do_if(expr) if (auto&& do_if = expr) do_if

#endif
